(function(){

	'use strict'

	var vendorsListController = function(vendorsList) {
		var list = this;
		list.vendors = vendorsList;
		
		function init() {
			
		};

		init();
	};


	var vendorsEditorController = function(vendor, vendorsService, messageBoxService, $state, auxFunctions) {
		var editor = this;
		vendor ? editor.vendor = vendor : $state.go('app.lc.vendors.list');

		editor.vendorCreate = function(vendor){
			editor.loading = true;
			var newVendor = angular.copy(vendor);
			/*vendor.currencies.forEach(function(currency){
				if(currency.$$hashKey){
					delete currency.$$hashKey
				}
			})*/
			vendorsService.vendorCreate(newVendor).then(function(data) {
				if(data){
					messageBoxService.showSuccess('Proveedor creado correctamente.');
					$state.go('app.lc.vendors.list')
					editor.loading = false;
				}
			})
		}

		editor.vendorUpdate = function(vendor){
			editor.loading = true;
			var newVendor = angular.copy(vendor);

			vendorsService.vendorUpdate(newVendor).then(function(data) {
				if(data){
					messageBoxService.showSuccess('Proveedor actualizado correctamente.');
				}
				$state.go('app.lc.vendors.list')
				editor.loading = false;
			})
		}

		editor.vendorDelete = function(){
			editor.deleting = true;
			vendorsService.vendorRemove(editor.vendor).then(function(data) {
				if(data){
					messageBoxService.showSuccess('Proveedor eliminado correctamente.');
				}else{
					messageBoxService.showError('Ocurrió un error inesperado.');
				}
				$state.go('app.lc.vendors.list');
				editor.deleting = false;
			})
		}

		editor.newField = {};
		editor.editing = false;

		editor.addElement = function(collection, element){
			auxFunctions.addElement(collection, element);
			editor.edit();
			editor.editInline(element)
		}

		editor.removeElement = function(collection, element){
			auxFunctions.removeElement(collection, element)
		}

		editor.editInline = function(field) {
			editor.editing = editor.vendor.currencies.indexOf(field);
			editor.newField = angular.copy(field);
		}

		editor.edit = function(){
			editor.editMode = !editor.editMode;
		}

		editor.saveField = function(index, sale) {
			if (editor.editing !== false) {
				editor.editing = false;
				editor.edit();
			}       
		};

		editor.cancel = function(index) {
			if (editor.editing !== false) {
				editor.vendor.currencies[editor.editing] = editor.newField;
				if(jQuery.isEmptyObject(editor.vendor.currencies[editor.editing])){
					editor.removeElement(editor.vendor.currencies, editor.vendor.currencies[editor.editing]);
				}
				editor.editing = false;
				editor.edit();
			}
		};

		function init () {
			
		};

		init();
	}

	angular.module('vendors')
		.controller('vendorsListController', vendorsListController)
		.controller('vendorsCreationController', vendorsEditorController)
		.controller('vendorsEditionController', vendorsEditorController)

})();