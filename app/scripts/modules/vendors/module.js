(function(){
	'use strict'
	angular.module('vendors', [])

	.config(function ($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('app.lc.vendors', {
				url: '/proveedores',
				abstract : true,
				template: '<ui-view/>'
			})

			.state('app.lc.vendors.list', {
				url: '/lista',
				templateUrl: '/scripts/modules/vendors/views/list.html',
				controller: 'vendorsListController as list',
				resolve : {
					vendorsList : function(vendorsService) {
						return vendorsService.retrieveAll()
					}
				}
			})

			.state('app.lc.vendors.create', {
				url: '/crear/',
				templateUrl: '/scripts/modules/vendors/views/vendor-editor.html',
				controller: 'vendorsCreationController as editor',
				resolve : {
					vendor : function() {
						return {}
					}
				}
    		})

			.state('app.lc.vendors.editor', {
				url: '/editar/:vendorId',
				templateUrl: '/scripts/modules/vendors/views/vendor-editor.html',
				controller: 'vendorsEditionController as editor',
				resolve : {
					vendor : function(vendorsService, $stateParams) {
						var vendorId = $stateParams.vendorId;
						return vendorsService.getVendorById(vendorId)
					}
				}
			})
	})
})()
