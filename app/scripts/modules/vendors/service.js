(function(){

	'use strict'

	var vendorsService = function(fireRef, Kutral, $q, $firebaseArray) {
		var service = this;

		var kutral = new Kutral(fireRef);
		var vendorSchema;

		var vendorModel;

		vendorSchema = new Kutral.Schema({
			'code': {type: Number},
			'name': {type: String, indexOn: true}
		});

		vendorModel = kutral.model('vendors', vendorSchema);

		service.retrieveAll = function(){
			var vendorRetriveAllPromise = $q.defer(),
				ref = $firebaseArray(new Firebase(fireRef + 'vendors'));

			ref.$loaded(function(data) {
				vendorRetriveAllPromise.resolve(data);
			});

			return vendorRetriveAllPromise.promise;
		}

		service.getVendorById = function(id){
			var vendorGetvendorByIdPromise = $q.defer();
			vendorModel.find({$id:id}, function(data) {
				vendorGetvendorByIdPromise.resolve(data);
			});
			return vendorGetvendorByIdPromise.promise;
		}

		service.getVendorByName = function(name){
			var vendorGetVendorByNamePromise = $q.defer(),
				ref = $firebaseArray(new Firebase(fireRef + 'vendors')),
				vendorFinded;

			ref.$loaded(function(data) {
				data.forEach(function(vendor){
					if(vendor.name.toLowerCase() == name.toLowerCase()){
						vendorFinded = vendor
						return;
					}
				})

				vendorGetVendorByNamePromise.resolve(vendorFinded);
				
			});

			return vendorGetVendorByNamePromise.promise;
		}

		service.vendorCreate = function(vendor){
			var vendorCreatePromise = $q.defer(),
				ref = $firebaseArray(new Firebase(fireRef + 'vendors'));

			ref.$add(vendor).then(function(data) {
				vendorCreatePromise.resolve(data.key());
			}, function(error) {
				console.log("Error:", error);
			});

			return vendorCreatePromise.promise;
		}

		service.vendorUpdate = function(vendor){
			var vendorUpdatePromise = $q.defer();

			vendorModel.data = vendor;
			vendorModel.update(function(success) {
				if(success){
					vendor = vendorModel.data;
				}
				vendorUpdatePromise.resolve(vendorModel.data);
			})

			return vendorUpdatePromise.promise;
		}

		service.vendorRemove = function(vendor){
			var vendorRemovePromise = $q.defer();
			if(vendor.id){ var id = vendor.id };
			if(vendor.$id){ var id = vendor.$id };

			vendorModel.data = vendor;
			vendorModel.remove({$id: id},function(success){
				if(success){
					vendor = vendorModel.data;
				}
				vendorRemovePromise.resolve(success);
			})

			return vendorRemovePromise.promise;
		}
	}

	angular.module('vendors')
		.service('vendorsService', vendorsService)
})();