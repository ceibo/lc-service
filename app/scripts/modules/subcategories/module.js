(function(){
	'use strict'
	angular.module('subcategories', [])

	.config(function ($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('app.lc.subcategories', {
				url: '/subcategorias',
				abstract : true,
				template: '<ui-view/>'
			})

			.state('app.lc.subcategories.list', {
				url: '/lista',
				templateUrl: '/scripts/modules/subcategories/views/list.html',
				controller: 'subcategoriesListController as list',
				resolve : {
					subcategoriesList : function(subcategoriesService) {
						return subcategoriesService.retrieveAll()
					}
				}
			})

			.state('app.lc.subcategories.editor', {
				url: '/editar/:subcategoryId',
				templateUrl: '/scripts/modules/subcategories/views/subcategory-editor.html',
				controller: 'subcategoryEditorCtrl as editor',
				resolve : {
					subcategory : function(subcategoriesService, $stateParams) {
						var subcategoryId = $stateParams.subcategoryId;
						return subcategoriesService.getSubcategoryById(subcategoryId)
					},
					categories : function(categoriesService) {
						return categoriesService.retrieveAll()
					}
				}
			})

			.state('app.lc.subcategories.create', {
				url: '/crear',
				templateUrl: '/scripts/modules/subcategories/views/subcategory-editor.html',
				controller: 'subcategoryCreationCtrl as editor',
				resolve: {
					subcategory : function() {
						return {}
					},
					categories : function(categoriesService) {
						return categoriesService.retrieveAll()
					}
				}
			})

			.state('app.lc.subcategories.import', {
				url: '/importar',
				templateUrl: '/scripts/modules/subcategories/views/import.html'
			})
	})
})()
