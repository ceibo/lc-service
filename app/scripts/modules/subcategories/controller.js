(function(){

	'use strict'

	var subcategoriesListController = function(subcategoriesList, subcategoriesService, messageBoxService) {
		var list = this;

		list.remove = function(subcategory){
			list.deleteProcess = true;
			subcategoriesService.remove(subcategory).then(function(data){
				messageBoxService.showSuccess('Subcategoría eliminada correctamente.');
				list.deleteProcess = false;
			})
		};

		function init() {
			list.subcategories = subcategoriesList;
		};

		init();
	};

	var subcategoryEditorController = function(subcategory, categories, subcategoriesService, $state, messageBoxService) {
		var editor = this;

		editor.save = function(subcat){
			editor.loading = true;
			subcategoriesService.subcategoryCreate(subcat).then(function(data) {
				if(data){
					messageBoxService.showSuccess('Subcategoría creada correctamente.');
					$state.go('app.lc.subcategories.list')
					editor.loading = false;
				}else{

				}
			})
		}

		editor.update = function(subcat){
			editor.loading = true;
			subcategoriesService.subcategoryUpdate(subcat).then(function(data) {
				if(data){
					messageBoxService.showSuccess('Subcategoría actualizada correctamente.');
					$state.go('app.lc.subcategories.list')
					editor.loading = false;
				}
			})

			
		}

		function init() {
			editor.subcategory = subcategory;
			editor.categories = categories;
		};

		init();
	};

	angular.module('subcategories')
		.controller('subcategoriesListController', subcategoriesListController)
		.controller('subcategoryCreationCtrl', subcategoryEditorController)
		.controller('subcategoryEditorCtrl', subcategoryEditorController)
})();