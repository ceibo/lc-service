(function(){

	'use strict'

	var subcategoriesService = function(fireRef, Kutral, $q, $firebaseArray) {	
		var service = this;

		var kutral = new Kutral(fireRef);
		var subcategorySchema;

		var subcategoryModel;

		subcategorySchema = new Kutral.Schema({
			'title': {type: String, indexOn: true}
		});

		subcategoryModel = kutral.model('subcategories', subcategorySchema);

		service.getSubcategoryById = function(id){
			var getSubcategoryByIdPromise = $q.defer();
			
			subcategoryModel.find({$id:id}, function(data){
				getSubcategoryByIdPromise.resolve(data);
			});

			return getSubcategoryByIdPromise.promise;
		}

		service.retrieveAll = function(){
			var subcategoryRetriveAllPromise = $q.defer();

			var ref = new Firebase(fireRef + 'subcategories');
			subcategoryRetriveAllPromise.resolve($firebaseArray(ref));

			return subcategoryRetriveAllPromise.promise;
		}

		service.subcategoryCreate = function(subcategory){
			var subcategoryCreatePromise = $q.defer(),
				ref = $firebaseArray(new Firebase(fireRef + 'subcategories'));
				
			subcategory.title = subcategory.title.toUpperCase();
			ref.$add(subcategory).then(function(data) {
				subcategoryCreatePromise.resolve(data.key());
			}, function(error) {
				console.log("Error:", error);
			});

			return subcategoryCreatePromise.promise;
		}

		service.getSubcategoryByTitle = function(title){
			var getSubcategoryByTitlePromise = $q.defer(),
				ref = $firebaseArray(new Firebase(fireRef + 'subcategories')),
				categoryFinded;

			ref.$loaded(function(data) {
				data.forEach(function(category){
					if(category.title.toLowerCase() == title.toLowerCase()){
						categoryFinded = category;
						return;
					}
				})

				getSubcategoryByTitlePromise.resolve(categoryFinded);
				
			});

			return getSubcategoryByTitlePromise.promise;
		}

		service.subcategoryUpdate = function(subcategory){
			var subcategoryUpdatePromise = $q.defer();

      		subcategoryModel.data = subcategory;
      		subcategoryModel.update(function(success) {
				if(success){
					subcategory = subcategoryModel.data;
				}
				subcategoryUpdatePromise.resolve(subcategoryModel.data);
			})

			return subcategoryUpdatePromise.promise;
		}

		service.remove = function(id){
			var subcategoryRemovePromise = $q.defer();

        	subcategoryModel.remove({$id: id},function(success) {
				subcategoryRemovePromise.resolve();
			})

			return subcategoryRemovePromise.promise;
		}
	}

	angular.module('subcategories')
		.service('subcategoriesService', subcategoriesService)
})();