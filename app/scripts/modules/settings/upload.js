'use strict';

angular.module('lascoloradasApp')

.service('uploadSettings', function($q, fireRef, Kutral, $firebaseArray, fireService) {
    var service = this;

    var kutral = new Kutral(fireRef);
	var uploadSchema;

	var uploadModel;

	uploadSchema = new Kutral.Schema({
		//'name': {type: String, indexOn: true}
	});

	uploadModel = kutral.model('jobs/cleanup', uploadSchema);

    service.getSettings = function(type) {

    	return fireService.findAndGetObject('settings', type, 'object');

    };

    /*service.deleteFile = function(cloudId) {

    	fireService.initAndGetObject('jobs', Date.now(), cloudId, 'cleanup');

    };*/

	service.deleteFile = function(cloudId){
		var deletePromise = $q.defer();

		uploadModel.data = cloudId;
		uploadModel.create(function(success) {
			if(success){
				cloudId = uploadModel.data;
			}

			deletePromise.resolve(uploadModel.data);
		})

		return deletePromise.promise;
	}

    return service;
});
