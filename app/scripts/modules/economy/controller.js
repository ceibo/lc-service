(function(){

	'use strict'

	var shoppingsController = function(shp, vendors, brands, products, vendorsService, brandsService, productsService, economyService, messageBoxService, auxFunctions, $state) {
		var shopping = this;
		shopping.product = shp;
		shopping.vendors = vendors;
		shopping.brands = brands;
		shopping.products = products;

		shopping.vendorsService = {
			get: vendorsService.getVendorById
		};
		shopping.brandsService = {
			get: brandsService.getBrandById
		};
		shopping.productsService = {
			get: productsService.getProductById
		};

		shopping.selectType = [{'value':'Factura'},{'value':'Consignación'}];

        shopping.findProduct = function(ean, index){
            if(ean && ean.toString().length === 13){
                productsService.searchByEAN(ean).then(function(data) {
                    if(data){
                    	var brandTitle = brandsService.getBrandById(data.brand).then(function(brand) {
	                        shopping.product.products[index] = {
	                        	product: { title : data.title },
	                        	ean: data.ean,
	                        	brand: brand.title,
	                        	price: data.price
	                        };
                    	});
                        shopping.findProd = true;
                    } else {
                        shopping.findProd = false;
                    }
                });
            }
        };

		// table
		shopping.newField = {};
		shopping.editing = false;

		shopping.addElement = function(collection, element){
			auxFunctions.addElement(collection, element);
			shopping.edit();
			shopping.editInline(element);
		};

		shopping.removeElement = function(collection, element){
			auxFunctions.removeElement(collection, element);
		};

		shopping.editInline = function(field) {
			shopping.editing = shopping.product.products.indexOf(field);
			shopping.newField = _.clone(field);
		};

		shopping.edit = function(){
			shopping.editMode = !shopping.editMode;
		};

		shopping.saveField = function(index, sale) {
			if (shopping.editing !== false) {
				shopping.editing = false;
				shopping.edit();
			}
		};

		shopping.cancel = function(index) {
			if (shopping.editing !== false) {
				shopping.product.products[shopping.editing] = shopping.newField;
				if(jQuery.isEmptyObject(shopping.product.products[shopping.editing])){
					shopping.removeElement(shopping.product.products, shopping.product.products[shopping.editing]);
				}
				shopping.editing = false;
				shopping.edit();
			}
		};

		shopping.create = function(data){
			shopping.loading = true;
			var newData = angular.copy(data)
			economyService.createNewShopping(newData)
				.then(function(data){
					messageBoxService.showSuccess('Compra creada correctamente.');
					$state.go('app.lc.economy.shoppings.list')
					shopping.loading = false;
				}, function(error){
					messageBoxService.showError('Error.');
			 		$state.go('app.lc.economy.shoppings.list')
			 		shopping.loading = false;
			 	})

		}

		shopping.update = function(data){
			shopping.loading = true;
			var newData = _.clone(data);
			 economyService.updateShopping(newData)
			 	.then(function(data){
			 		messageBoxService.showSuccess('Compra actualizada correctamente.');
			 		$state.go('app.lc.economy.shoppings.list')
			 		shopping.loading = false;
			 	}, function(error){
					messageBoxService.showError('Error.');
			 		$state.go('app.lc.economy.shoppings.list')
			 		shopping.loading = false;
			 	})

		}

		shopping.delete = function(data){
			shopping.deleting = true;
			var newData = _.clone(data);
			 economyService.deleteShopping(newData)
			 	.then(function(data){
			 		messageBoxService.showSuccess('Compra eliminada correctamente.');
			 		$state.go('app.lc.economy.shoppings.list')
			 		shopping.deleting = false;
			 	}, function(error){
			 		messageBoxService.showError('Error.');
			 		$state.go('app.lc.economy.shoppings.list')
			 		shopping.deleting = false;
			 	})
		}

		shopping.setTotal = function(price, quantity, item){
			if(price && quantity){
				item.total = price * quantity;
				item.total = Math.round(item.total * 100) / 100;
			}else{
				item.total = price || quantity;
			}
		}

		shopping.setProductsFromVendor = function(vendor){
			// console.log(vendor)
		}

		function init () {
			if(!shopping.product.products) shopping.product.products = [];
		};

		init();
	}

	var shoppingsListController = function(shoppingsList, vendorsService) {
		var list = this;

		list.shoppings = shoppingsList;

		list.vendorsService = {
			get: vendorsService.getVendorById
		}

		function init() {

		};

		init();
	}

	angular.module('economy')
		.controller('shoppingsController', shoppingsController)
		.controller('shoppingsListController', shoppingsListController)

})();
