(function(){
	'use strict'
	angular.module('economy', [])

	.config(function ($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('app.lc.economy', {
				url: '/economia',
				abstract : true,
				template: '<ui-view/>'
			})

			.state('app.lc.economy.shoppings', {
				url: '/compras',
				abstract : true,
				template: '<ui-view/>'
			})

			.state('app.lc.economy.shoppings.list', {
				url: '/lista/',
				templateUrl: '/scripts/modules/economy/views/shoppings/list.html',
				controller: 'shoppingsListController as list',
				resolve : {
					shoppingsList : function(economyService) {
						return economyService.retrieveAll()
					}
				}
			})

			.state('app.lc.economy.shoppings.create', {
				url: '/crear/',
				templateUrl: '/scripts/modules/economy/views/shoppings/shoppings.html',
				controller: 'shoppingsController as shopping',
				resolve : {
					shp : function() {
						return {}
					},
					vendors : function(vendorsService) {
						return vendorsService.retrieveAll()
					},
					brands : function(brandsService) {
						return brandsService.retrieveAll()
					},
					products : function(productsService) {
						return productsService.retrieveAll()
					}
				}
			})

			.state('app.lc.economy.shoppings.editor', {
				url: '/editar/:shoppingId',
				templateUrl: '/scripts/modules/economy/views/shoppings/shoppings.html',
				controller: 'shoppingsController as shopping',
				resolve : {
					shp : function(economyService, $stateParams) {
						var id = $stateParams.shoppingId;
						return economyService.getShoppingById(id)
					},
					vendors : function(vendorsService) {
						return vendorsService.retrieveAll()
					},
					brands : function(brandsService) {
						return brandsService.retrieveAll()
					},
					products : function(productsService) {
						return productsService.retrieveAll()
					}
				}
			})

	})
})()
