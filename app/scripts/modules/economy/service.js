(function(){

	'use strict'

	var economyService = function(fireRef, Kutral, $q, $firebaseArray, $firebaseObject, vendorsService, brandsService, productsService, expensesService, momentFunctions) {
		var service = this;

		var kutral = new Kutral(fireRef);
		var shoppingSchema;

		var shoppingModel;

		shoppingSchema = new Kutral.Schema({
			
		});

		shoppingModel = kutral.model('shopping', shoppingSchema);

		var shoppingRef = $firebaseArray(new Firebase(fireRef + 'shopping'));

		service.retrieveAll = function(){
			var retrieveAllPromise = $q.defer();

			var ref = $firebaseArray(new Firebase(fireRef + 'shopping'));
				ref.$loaded(function(data) {
					retrieveAllPromise.resolve(data);
				});

			return retrieveAllPromise.promise;
		}

		service.getShoppingById = function(id){
			var getShoppingByIdPromise = $q.defer();

			var ref = $firebaseObject(new Firebase(fireRef + 'shopping/' + id));
				ref.$loaded(function(data) {
					getShoppingByIdPromise.resolve(data);
				});

			// shoppingModel.find({$id:id}, function(data){
			// 	console.log(data)
			// 				getShoppingByIdPromise.resolve(data);
			// 			});

			return getShoppingByIdPromise.promise;
		}

		service.createNewShopping = function(data) {
			var createNewShoppingPromise = $q.defer(), vendorPromise = $q.defer(), brandPromise = [], productPromise = [], expensePromise = $q.defer();
			data.total = 0;
			data.date = momentFunctions.getDate();
			//check vendor
			if(data.vendor.name){
				//existe
				data.vendor = data.vendor.id || data.vendor.$id;
				vendorPromise.resolve(data.vendor);
			}else{
				//no existe, busco por nombre
				vendorsService.getVendorByName(data.vendor).then(function(vendor) {
					if(!vendor){
						// lo creo
						var vendorToCreate = {name: data.vendor}
						vendorsService.vendorCreate(vendorToCreate).then(function(newVendor) {
							data.vendor = newVendor;
							vendorPromise.resolve(data.vendor);
						})
					}else{
						data.vendor = vendor.id || vendor.$id;
						vendorPromise.resolve(data.vendor);
					}
				})

			}

			$q.all(vendorPromise).then(function() {

				data.products.forEach(function(product, i) {
					var loopPromise = $q.defer();
					//check brand
					if(product.brand.$id){
						//existe
						product.brand = product.brand.$id;
						loopPromise.resolve(product.brand);
					}else{
						// no existe
						brandsService.getBrandByTitle(product.brand).then(function(brand) {
							if(!brand){
								// la creo
								var brandToCreate = {title: product.brand}
								brandsService.brandCreate(brandToCreate).then(function(newBrand) {
									product.brand = newBrand;
									loopPromise.resolve(product.brand);
								})
							}else{
								product.brand = brand.id || brand.$id;
								loopPromise.resolve(product.brand);
							}
						})

					}
					brandPromise.push(loopPromise.promise);
				})
				
				$q.all(brandPromise).then(function() {
					data.products.forEach(function(product, i){
						data.total += product.total;
						var loopPromise = $q.defer();
						//check product
						if(product.product.$id){
							//existe, sumo stock
							productsService.getProductById(product.product.$id, product.product.status).then(function(data){
								product.product = product.product.$id;
								data.stock += product.quantity;
								productsService.updateProduct(data).then(function(data){
									loopPromise.resolve(product.product);
								})
							})
						}else{
							// no existe, creo producto
							var productToCreate = {title: product.product, brand: product.brand, vendor: data.vendor, stock: product.quantity, cost: product.price, price: 0}
							
							productsService.createProduct(productToCreate, 'pending').then(function(newProduct){
								product.product = newProduct.id;
								loopPromise.resolve(product.product);
							})
						}
						productPromise.push(loopPromise.promise)
					})

					$q.all(productPromise).then(function() {
						var expenseToCreate = {total: data.total, date: data.date}
						expensesService.expensesCreate(expenseToCreate).then(function(expense){
							data.expense = expense.id;
							expensePromise.resolve(data.expense);
						})

						$q.all(expensePromise).then(function() {
							// creo una nueva venta
							shoppingRef.$add(data).then(function(data) {
								createNewShoppingPromise.resolve(data.key());
							}, function(error) {
								console.log("Error:", error);
								createNewShoppingPromise.reject(error);
							});
						})
					})		
				})
			})

			return createNewShoppingPromise.promise;
		}

		service.updateShopping = function(data) {
			var updateShoppingPromise = $q.defer(), vendorPromise = $q.defer(), brandPromise = [], productPromise = [], expensePromise = $q.defer();
			data.total = 0;
			var ref = $firebaseObject(new Firebase(fireRef + 'shopping/' + data.$id));

			ref.$loaded().then(function(oldValue){
				//check vendor
				if(data.vendor.name){
					//existe
					data.vendor = data.vendor.id || data.vendor.$id;
					vendorPromise.resolve(data.vendor);
				}else{
					//no existe, busco por nombre
					vendorsService.getVendorByName(data.vendor).then(function(vendor) {
						if(!vendor){
							// lo creo
							var vendorToCreate = {name: data.vendor}
							vendorsService.vendorCreate(vendorToCreate).then(function(newVendor) {
								data.vendor = newVendor;
								vendorPromise.resolve(data.vendor);
							})
						}else{
							data.vendor = vendor.id || vendor.$id;
							vendorPromise.resolve(data.vendor);
						}
					})

				}

				$q.all(vendorPromise).then(function() {

					data.products.forEach(function(product, i) {
						var loopPromise = $q.defer();
						//check brand
						if(product.brand.$id){
							//existe
							product.brand = product.brand.$id;
							loopPromise.resolve(product.brand);
						}else{
							// no existe
							if(product.brand.title){
								product.brand = product.brand.title;
							}
							brandsService.getBrandByTitle(product.brand).then(function(brand) {
								if(!brand){
									// la creo
									var brandToCreate = {title: product.brand}
									brandsService.brandCreate(brandToCreate).then(function(newBrand) {
										product.brand = newBrand;
										loopPromise.resolve(product.brand);
									})
								}else{
									product.brand = brand.id || brand.$id;
									loopPromise.resolve(product.brand);
								}
							})

						}
						brandPromise.push(loopPromise.promise);
					})
					
					$q.all(brandPromise).then(function() {
						data.products.forEach(function(product, i){
							data.total += product.total;
							var loopPromise = $q.defer();
							//check product
							if(product.product.$id){
								//existe, sumo stock
								productsService.getProductById(product.product.$id, product.product.status).then(function(prod){
									product.product = product.product.$id;
									if(oldValue.products[i]){
										if(oldValue.products[i].quantity != product.quantity){
											var dif = oldValue.products[i].quantity - product.quantity;
											prod.stock -= dif;

											productsService.updateProduct(prod).then(function(data){
												loopPromise.resolve(product.product);
											})
										}else{
											loopPromise.resolve(product.product);
										}
									}else{
										prod.stock += product.quantity;
										productsService.updateProduct(prod).then(function(data){
											loopPromise.resolve(product.product);
										})
									}
								})
							}else{
								// no existe, creo producto
								var productToCreate = {title: product.product, brand: product.brand, vendor: data.vendor, stock: product.quantity, cost: product.price, price: 0}
								
								productsService.createProduct(productToCreate, 'pending').then(function(newProduct){
									product.product = newProduct.id;
									loopPromise.resolve(product.product);
								})
							}
							productPromise.push(loopPromise.promise)
						})

						$q.all(productPromise).then(function() {

							var expenseRef = $firebaseObject(new Firebase(fireRef + 'expenses/' + data.expense));
							expenseRef.$loaded().then(function(expense){
								expense.total = data.total;

								expenseRef.$save().then(function(updatedExpense) {
								  expensePromise.resolve(updatedExpense.key());
								}, function(error) {
								  console.log("Error:", error);
								  expensePromise.resolve();
								});
							})
							$q.all(expensePromise).then(function() {

								oldValue = $.extend( oldValue, data )

								ref.$save().then(function(updatedShopping) {
								  updateShoppingPromise.resolve(updatedShopping.key());
								}, function(error) {
								  console.log("Error:", error);
								  updateShoppingPromise.reject(error);
								});
							})
						})		
					})
				})
			})

			return updateShoppingPromise.promise;
		}

		service.deleteShopping = function(data) {
			var deleteShoppingPromise = $q.defer(), expensePromise = $q.defer(), productPromise = [];

			var expenseRef = $firebaseObject(new Firebase(fireRef + 'expenses/' + data.expense));
				expenseRef.$loaded().then(function(expense){

					expenseRef.$remove().then(function(deletedExpense) {
						expensePromise.resolve(deletedExpense);
					}, function(error) {
						console.log("Error:", error);
						deleteShoppingPromise.reject(error);
					});
				})
				
				$q.all(expensePromise).then(function() {
					data.products.forEach(function(product, i){
							var loopPromise = $q.defer();							
							if(product.product.$id){
								productsService.getProductById(product.product.$id, product.product.status).then(function(prod){
									if(prod){
										prod.stock -= product.quantity;
										productsService.updateProduct(prod).then(function(data){
											loopPromise.resolve(product.product);
										})
									}else{
										loopPromise.reslove();
									}
								})
							}else{
								loopPromise.reslove();
							}
							productPromise.push(loopPromise.promise)
					})

					$q.all(productPromise).then(function() {
						var ref = $firebaseObject(new Firebase(fireRef + 'shopping/' + data.$id));

						ref.$loaded().then(function(oldValue){
							ref.$remove().then(function(deletedShopping) {
								deleteShoppingPromise.resolve(deletedShopping);
							}, function(error) {
								console.log("Error:", error);
								deleteShoppingPromise.reject(error);
							});
						})
					})

				})


			return deleteShoppingPromise.promise;	
		}

	}

	angular.module('economy')
		.service('economyService', economyService)
})();