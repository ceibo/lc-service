(function(){

	'use strict';

	var momentFunctionsService = function() {
		var service = this;

		moment.locale('es', {
			months : [
				"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
				"Agosto", "Septiempre", "Octubre", "Noviembre", "Diciembre"
			],
			monthsShort : [
				"Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul",
				"Ago", "Sep", "Oct", "Nov", "Dic"
			],
			weekdays : [
				"Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"
			],
			weekdaysShort : [
				"Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"
			],
			weekdaysMin : [
				"Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá", "Do"
			],
			longDateFormat : {
				LT : "HH:mm A",
				LTS : "HH:mm:ss A",
				L : "DD/MM/YYYY",
				LL : "D [de] MMMM[,] YYYY",
				LLL : "dddd D [de] MMMM[,] YYYY",
				LLLL : "dddd D [de] MMMM[,] YYYY LT"
			},
			calendar : {
				lastDay : '[Ayer a las] LT',
				sameDay : '[Hoy a las] LT',
				nextDay : '[Mañana a las] LT',
				lastWeek : '[El pasado] dddd [a las] LT',
				nextWeek : 'dddd [a las] LT',
				sameElse : 'L'
			},
			relativeTime : {
				future : "en %s",
				past : "hace %s",
				s : "unos pocos segundos",
				m : "un minuto",
				mm : "%d minutos",
				h : "une hora",
				hh : "%d horas",
				d : "un día",
				dd : "%d días",
				M : "un mes",
				MM : "%d meses",
				y : "un año",
				yy : "%d años"
			}
		})

		this.getDate = function(){
			var timeZone = moment.tz(moment().format(), "America/Argentina/Buenos_Aires");

			var date = {
				fullDateTime: timeZone.format('LLLL'),
				fullDate: timeZone.format('LLL'),
				calendar: timeZone.format('L'),
				time: timeZone.format('LT'),
				fullTime: timeZone.format('LTS'),
				year: timeZone.get('year'),
				month: timeZone.get('month'),
				week: timeZone.get('week'),
				day: timeZone.get('date'),
				hour: timeZone.get('hour'),
				minutes: timeZone.get('minutes'),
				dateInMilliseconds: moment(timeZone).valueOf(),
			};

			return date
		}
	}

	angular.module('utils')
		.service('momentFunctions', momentFunctionsService)

})();