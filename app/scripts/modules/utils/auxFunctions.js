(function(){

	'use strict';

	var utilsFunctionsService = function() {

		/* Generates a set from a given collection, useful for iterate over this set when we want to give an autocomplete component or similar */
		this.generateSet = function(collection, key, subKey) {
			var set = [];
			if(key && subKey) {
				angular.forEach(collection, function(element){
					if(angular.isArray(element[subKey])) {
						angular.forEach(element[subKey], function(subElement){
							if(set.indexOf(subElement[key]) == -1) {
								set.push(subElement[key]);
							};
						})
					} else {
						if(set.indexOf(element[subKey][key]) == -1 ) {
							set.push(element[subKey][key]);
						};
					}
				})
			} else if(key) {
				angular.forEach(collection, function(element){
					if(set.indexOf(element[key]) == -1) {
						set.push(element[key]);
					}
				})
			} else {
				angular.forEach(collection, function(element){
					if(set.indexOf(element) == -1) {
						set.push(element);
					}
				})
			}

			return set;
		}

		this.addElement = function(collection, element, property, options) {
				if(options && options.notAllowRepeated) {
					for (var i = 0; i < collection.length; i++) {
						if(collection[i][property] == element) {
							return false;
						}
					};
					collection.push(element)
				} else {
					collection.push(element)
				};

				return true
		}

		this.removeElement = function(collection, element) {
			var index = collection.indexOf(element);
			collection.splice(index, 1);
		}

		this.addElement = function(collection, element) {
			collection.push(element);
		}

		this.changeElementPositionToFirst = function(collection, elementToChange) {
			var elementToChangeIndex = collection.indexOf(elementToChange);
			var temporaryCopy = collection[0];
			collection[0] = elementToChange;
			collection[elementToChangeIndex] = temporaryCopy;
		}

		this.resultsToArray = function(data) {
			var results = [];
			if(data) {
				_.map(data, function(element) {
					results.push(element);
				});
			}
			
			return results;
		}

		this.uniq = function(a) {
			var prims = {"boolean":{}, "number":{}, "string":{}}, objs = [];

			return a.filter(function(item) {
				var type = typeof item;
				if(type in prims)
					return prims[type].hasOwnProperty(item) ? false : (prims[type][item] = true);
				else
					return objs.indexOf(item) >= 0 ? false : objs.push(item);
			});
		}

		// Convert array to object
		this.convArrToObj = function(array){
		    var thisEleObj = new Object();
		    if(typeof array == "object"){
		        for(var i in array){
		            var thisEle = this.convArrToObj(array[i]);
		            thisEleObj[i] = thisEle;
		        }
		    }else {
		        thisEleObj = array;
		    }
		    return thisEleObj;
		}

	}

	angular.module('utils')
		.service('auxFunctions', utilsFunctionsService)

})();