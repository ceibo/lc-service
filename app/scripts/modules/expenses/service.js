(function(){

	'use strict'

	var expensesService = function(fireRef, Kutral, $q, $firebaseArray) {
		var service = this;

		var kutral = new Kutral(fireRef);
		var expensesSchema;

		var expensesModel;

		expensesSchema = new Kutral.Schema({
			//
		});

		expensesModel = kutral.model('expenses', expensesSchema);

		service.retrieveAll = function(){
			var expensesRetriveAllPromise = $q.defer();

			var refExpenses = $firebaseArray(new Firebase(fireRef + 'expenses'));

			refExpenses.$loaded(function(data) {
				expensesRetriveAllPromise.resolve(data);
			});

			return expensesRetriveAllPromise.promise;
		}

		service.expensesCreate = function(expenses){
			var expensesCreatePromise = $q.defer();

			expensesModel.data = expenses;
			expensesModel.create(function(success) {
				if(success){
					expenses = expensesModel.data;
				}
				expensesCreatePromise.resolve(expensesModel.data);
			})
			return expensesCreatePromise.promise;
		}

		service.expensesUpdate = function(expenses){
			var expensesUpdatePromise = $q.defer();

			expensesModel.data = expenses;
			expensesModel.update(function(success) {
				if(success){
					expenses = expensesModel.data;
				}
				expensesUpdatePromise.resolve(expensesModel.data);
			})
			return expensesUpdatePromise.promise;
		}

	}

	angular.module('expenses')
		.service('expensesService', expensesService)
})();