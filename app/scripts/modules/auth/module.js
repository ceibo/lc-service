angular.module('auth', [])
 .config(['$stateProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('auth', {
        abstract:true,
        url:'/acceso',
        template:'<div ui-view="" class="ng-fadeOutZoom"></div>'
      })

      .state('auth.login',{
        url: '/entrar',
        templateUrl: '/scripts/modules/auth/views/login.html',
        controller: 'authCtrl as login',
        resolve : {
          user : function(AuthTokenService) {
            return AuthTokenService.isAuthorized();
          }
        }
      })

      /*.state('auth.signup', {
        url: '/signup',
        templateUrl: 'views/signup.html',
        controller: 'authCtrl as authCtrl'
      })

      .state('auth.password', {
        url: '/password',
        templateUrl: 'views/password.html',
        controller: 'authCtrl as authCtrl'
      })

      .state('auth.register', {
        url: '/registrarse',
        templateUrl: '/scripts/modules/auth/views/register.html',
        controller: 'authCtrl as register',
        resolve : {
          user : function(AuthTokenService) {
            return AuthTokenService.isAuthorized();
          }
        }
      });*/
  }]);
