(function(){

	'use strict'

	var aboutService = function(fireRef, Kutral, $q, $firebaseArray) {
		var service = this;

		var kutral = new Kutral(fireRef);
		var aboutSchema;

		var aboutModel;

		aboutSchema = new Kutral.Schema({
			
		});

		aboutModel = kutral.model('about', aboutSchema);

		service.retrieveAll = function(){
			var aboutRetriveAllPromise = $q.defer();

			var ref = $firebaseArray(new Firebase(fireRef + 'about'));

			ref.$loaded(function(data) {
				aboutRetriveAllPromise.resolve(data[0]);
			});

			return aboutRetriveAllPromise.promise;
		}

		service.aboutCreate = function(about){
			var aboutCreatePromise = $q.defer();

			aboutModel.data = about;
			aboutModel.create(function(success) {
				if(success){
					about = aboutModel.data;
				}
				aboutCreatePromise.resolve(aboutModel.data);
			})
			return aboutCreatePromise.promise;
		}

		service.aboutUpdate = function(about){
			var aboutUpdatePromise = $q.defer();

			aboutModel.data = about;
			aboutModel.update(function(success) {
				if(success){
					about = aboutModel.data;
				}
				aboutUpdatePromise.resolve(aboutModel.data);
			})

			return aboutUpdatePromise.promise;
		}

		service.removeElement = function(collection, element) {
			var index = collection.indexOf(element);
			collection.splice(index, 1);
		}
	}

	angular.module('about')
		.service('aboutService', aboutService)
})();