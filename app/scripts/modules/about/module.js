(function(){
	'use strict'
	angular.module('about', [])

	.config(function ($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('app.lc.about', {
				url: '/sobre-nosotros',
				abstract : true,
				template: '<ui-view/>'
			})

			.state('app.lc.about.create', {
				url: '/crear/',
				templateUrl: '/scripts/modules/about/views/about-editor.html',
				controller: 'aboutCreationController as editor',
				resolve : {
					about : function() {
						return {}
					}
				}
    		})

			.state('app.lc.about.editor', {
				url: '/editar/',
				templateUrl: '/scripts/modules/about/views/about-editor.html',
				controller: 'aboutEditionController as editor',
				resolve : {
					about : function(aboutService, $stateParams) {
						var aboutId = $stateParams.aboutId;
						return aboutService.retrieveAll()
					}
				}
			})
	})
})()
