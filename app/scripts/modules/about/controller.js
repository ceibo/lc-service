(function(){

	'use strict'

	var aboutEditorController = function(about, aboutService, $state, messageBoxService) {
		var editor = this;
		editor.about = about;

		editor.froalaOptions = {
			inlineMode: false,
			placeholder: "",
			buttons : ["bold", "italic", "underline", "strikeThrough", "fontSize", "color", "sep", "align", "insertOrderedList", "insertUnorderedList", "sep", "html"],
			events : {
				focus : function(e, editor) {/* ... */}
			}
		}

		editor.aboutCreate = function(about){
			editor.loading = true;
			aboutService.aboutCreate(about).then(function(data) {
				if(data){
					editor.loading = false;
				}
			})
		}

		editor.aboutUpdate = function(about){
			editor.loading = true;
			aboutService.aboutUpdate(about).then(function(data) {
				if(data){
					editor.loading = false;
					messageBoxService.showSuccess('Información actualizada correctamente.');
				}
			})
		}

		 function init () {
			
		};

		init();
	}

	angular.module('about')
		.controller('aboutCreationController', aboutEditorController)
		.controller('aboutEditionController', aboutEditorController)

})();