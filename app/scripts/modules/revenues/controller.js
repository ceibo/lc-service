(function(){

	'use strict'

	var revenuesCtrl = function(sales, expenses, momentFunctions) {
		var revenue = this;
		revenue.sales = sales;
		revenue.expenses = expenses;

		revenue.sales.forEach(function(data){
			data.type = "sale";
			data.total = 0;
			data.items.forEach(function(item){
			 	data.total += item.price * item.quantity;
			 })
		})

		revenue.expenses.forEach(function(data){
			data.type = "expense";
		})

		revenue.salesExpenses = revenue.sales.concat(revenue.expenses);
		revenue.salesExpenses.reverse();

		revenue.date = momentFunctions.getDate();
		
		revenue.getDataToday = function(){
			var today = [];

			revenue.salesExpenses.forEach(function(data){
			 	if(data.date.day == moment().get('date') && data.date.month == moment().get('month') && data.date.year == moment().get('year')){
			 		today.push(data)
			 	}
			})

			return today
		}

		revenue.getDataWeek = function(){
			var week = [];

			revenue.salesExpenses.forEach(function(data){
			 	
			 	if(data.date.week == moment().get('week') && data.date.month == moment().get('month') && data.date.year == moment().get('year')){
			 		week.push(data)
			 	}
			})

			return week
		}

		revenue.getDataMonth = function(){
			var month = [];
			
			revenue.salesExpenses.forEach(function(data){
			 	if(data.date.month == moment().get('month') && data.date.year == moment().get('year')){
			 		month.push(data)
			 	}
			})

			return month
		}

		revenue.setToday = function(){
			revenue.rowCollection = revenue.getDataToday();
			revenue.label = "de hoy";
		}
		revenue.setWeek = function(){
			revenue.rowCollection = revenue.getDataWeek();
			revenue.label = "de la semana";
		}
		revenue.setMonth = function(){
			revenue.rowCollection = revenue.getDataMonth();
			revenue.label = "del mes";
		}
		revenue.setTotal = function(){
			revenue.rowCollection = revenue.sales;
			revenue.rowCollection = revenue.rowCollection.concat(revenue.expenses)
			revenue.label = "totales";
		}

		revenue.getRevenue = function(items){
			var total = 0;
			if(items && items.length){
				items.forEach(function(item){
					if(item.type == 'sale'){
						item.items.forEach(function(item){
							total += item.price * item.quantity;
						})
					}
					if(item.type == 'expense'){
						total -= item.total;
					}
				})
			}

			return total
		}

		function init() {
			revenue.rowCollection = [];

			revenue.list = [].concat(revenue.rowCollection);

			revenue.today = revenue.getDataToday();
			revenue.week = revenue.getDataWeek();
			revenue.month = revenue.getDataMonth();
			revenue.rowCollection = revenue.today;
			revenue.label = "de hoy";

			revenue.revenueToday = revenue.getRevenue(revenue.today);
			revenue.revenueWeek = revenue.getRevenue(revenue.week);
			revenue.revenueMonth = revenue.getRevenue(revenue.month);

			revenue.revenueTotal = revenue.getRevenue(revenue.salesExpenses);
		};

		init();
	};

	angular.module('revenues')
		.controller('revenuesCtrl', revenuesCtrl)

})();