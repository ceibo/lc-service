(function(){
	'use strict'
	angular.module('revenues', [])

	.config(function ($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('app.lc.revenues', {
				url: '/ingresos',
				templateUrl: '/scripts/modules/revenues/views/revenues.html',
				controller : 'revenuesCtrl as revenue',
				resolve: {
					sales: function(salesService){
						return salesService.retrieveSuccess()
					},
					expenses: function(expensesService){
						return expensesService.retrieveAll()
					}
				}
			})
	})
})()
