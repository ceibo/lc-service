'use strict';

var listPhotoCtrl= function(uploadSettings, aboutService, Upload, $timeout) {
    var listCtrl = this;
    var _uploadSettings = uploadSettings.getSettings('upload');

    listCtrl.progress = 0;

    listCtrl.validate = function($file) {
        if ($file) {
            try {
                if (_uploadSettings.allowedType.indexOf($file.type) === -1) {
                    throw new Error('Only JPG, PNG, GIF Files are allowed');
                }
                if ($file.size > _uploadSettings.maxSize) {
                    throw new Error('File must not exceed 2Mb');
                }
                return $file;
            } catch (error) {
                listCtrl.error = error.message;
            }
        }
    };

    listCtrl.deleteFile = function(collection, element){
        var id = element.replace(/^.*\/(.*)$/, "$1");
        id = id.substr(0, id.lastIndexOf('.')) || id;
        uploadSettings.deleteFile(id).then(function(data){
            if(listCtrl.object){
                aboutService.aboutUpdate(listCtrl.object).then(function(data) {

                })
            }
        })

        aboutService.removeElement(collection, element)
    }

    listCtrl.upload = function() {
        if (listCtrl.file[0]) {
            var uploadParams = {
                upload_preset: _uploadSettings.presets.slider
            };
            //var oldListId = listCtrl.listId;
            listCtrl.progress = 0;
            listCtrl.uploadPromise = Upload.upload({
                url: _uploadSettings.url,
                method: 'POST',
                fields: uploadParams,
                file: listCtrl.file[0]
            }).success(function(data) {
                //listCtrl.listUrl = data.secure_url;
                //listCtrl.listId = data.public_id;
                //listCtrl.preview = listCtrl.listUrl;
                if (listCtrl.listUrl == undefined) {
                    listCtrl.listUrl = [];
                    listCtrl.listUrl.push(data.secure_url);
                } else {
                    listCtrl.listUrl.push(data.secure_url);
                }

                $timeout(function(){
                    listCtrl.progress = 0;
                }, 5000); 

                if(listCtrl.object){
                    aboutService.aboutUpdate(listCtrl.object).then(function(data) {                

                    })
                }

            }).error(function(err) {
                listCtrl.error = err.error.message;
            }).progress(function(evt) {
                listCtrl.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        }
    };
};

angular.module('lc.serv.ui.list-photo-uploader', [])
    .controller('listPhotoCtrl', listPhotoCtrl)
    .directive('listPhoto', function() {
        // Runs during compile
        return {
            scope: {
                listUrl: '=',
                listId: '=',
                model: '=',
                object: '=',
                onUploadedPromise: '=',
                listFormly : '@',
                showPanel : '@'
            },
            controller: 'listPhotoCtrl as list',
            // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
            restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
            templateUrl: '../scripts/modules/lc.serv.ui/list-photo/list.html',
            bindToController: true
        };
    });