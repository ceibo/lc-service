'use strict';

var producPhotoCtrl = function(uploadSettings, Upload, $timeout) {

    var photoCtrl = this;
    var _uploadSettings = uploadSettings.getSettings('upload');

    photoCtrl.progress = 0;

    photoCtrl.validate = function($file) {
        if ($file) {
            try {
                if (_uploadSettings.allowedType.indexOf($file.type) === -1) {
                    throw new Error(gettext('Only JPG, PNG, GIF Files are allowed'));
                }
                if ($file.size > _uploadSettings.maxSize) {
                    throw new Error(gettext('File must not exceed 2Mb'));
                }
                return $file;
            } catch (error) {
                photoCtrl.error = error.message;
            }

        }
    };

    photoCtrl.deleteFile = function(photoId){
        uploadSettings.deleteFile(photoId).then(function(data){
            if(photoCtrl.photoSliderId == data){
                photoCtrl.photoSliderUrl = '';
                photoCtrl.photoSliderId = null;
            }else{
                photoCtrl.photoUrl = '';
                photoCtrl.photoId = null;
            }
        })
    }

    photoCtrl.upload = function() {
        if (photoCtrl.file[0]) {
            var uploadParams = {
                upload_preset: _uploadSettings.presets.products
            };
            var oldPhotoId = photoCtrl.photoId;
            var oldPhotoUrl = photoCtrl.photoUrl;
            photoCtrl.progress = 0;
            photoCtrl.uploadPromise = Upload.upload({
                url: _uploadSettings.url,
                method: 'POST',
                fields: uploadParams,
                file: photoCtrl.file[0]
            }).success(function(data) {
                photoCtrl.photoUrl = data.url;
                photoCtrl.photoId = data.public_id;
                if (oldPhotoId && oldPhotoUrl) {
                    uploadSettings.deleteFile(oldPhotoId);
                }
                $timeout(function(){
                	photoCtrl.progress = 0;
                }, 5000);

            }).error(function(err) {
                photoCtrl.error = err.error.message;
            }).progress(function(evt) {
                // Math.min is to fix IE which reports 200% sometimes
                photoCtrl.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        }
    };

    photoCtrl.uploadSlider = function() {
        if (photoCtrl.file[0]) {
            var uploadParams = {
                upload_preset: _uploadSettings.presets.slider
            };
            var oldPhotoId = photoCtrl.photoSliderId;
            var oldPhotoUrl = photoCtrl.photoSliderUrl;
            photoCtrl.progress = 0;
            photoCtrl.uploadPromise = Upload.upload({
                url: _uploadSettings.url,
                method: 'POST',
                fields: uploadParams,
                file: photoCtrl.file[0]
            }).success(function(data) {
                photoCtrl.photoSliderUrl = data.secure_url;
                photoCtrl.photoSliderId = data.public_id;
                if (oldPhotoId && oldPhotoUrl) {
                    uploadSettings.deleteFile(oldPhotoId);
                }
                $timeout(function(){
                    photoCtrl.progress = 0;
                }, 5000);

            }).error(function(err) {
                photoCtrl.error = err.error.message;
            }).progress(function(evt) {
                // Math.min is to fix IE which reports 200% sometimes
                photoCtrl.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        }
    };
};

angular.module('lc.serv.ui.photo-uploader', [])
    .controller('producPhotoCtrl', producPhotoCtrl)
    .directive('productPhoto', function(uploadSettings, Upload) {
        // Runs during compile
        return {
            scope: {
                photoUrl: '=',
                photoId: '=',
                productId: '='
            }, // {} = isolate, true = child, false/undefined = no change
            controller: 'producPhotoCtrl as photoUploader',
            // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
            restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
            templateUrl: '../scripts/modules/lc.serv.ui/product-photos/product-photos.html',
            bindToController: true
        };
    })

    .directive('productPhotoSlider', function(uploadSettings, Upload) {
        // Runs during compile
        return {
            scope: {
                photoSliderUrl: '=',
                photoSliderId: '=',
                productId: '='
            }, // {} = isolate, true = child, false/undefined = no change
            controller: 'producPhotoCtrl as photoUploader',
            // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
            restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
            templateUrl: '../scripts/modules/lc.serv.ui/product-photos/product-photos-slider.html',
            bindToController: true
        };
    });
