'use strict';

angular.module('lc.serv.ui.photo-uploader')

.service('uploadSettings', function($http, fireService, AuthTokenService, Kutral) {
    var service = this;
    var jobs = new Kutral();
    service.getSettings = function(type) {

    	return fireService.findAndGetObject('settings', type, 'object');

    };

    service.deleteFile = function(cloudId) {
    	//fireService.initAndGetObject('jobs', Date.now(), cloudId, 'cleanup');
    	fireService.initAndGetObject('jobs', Date.now(), 'cleanup');
    };

    return service;
});