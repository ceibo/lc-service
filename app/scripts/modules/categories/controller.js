(function(){

	'use strict'

	var categoriesListController = function(categoriesList, categoriesService, subcategoriesService, messageBoxService) {
		var list = this;
		list.categories = categoriesList;

		list.subcategoriesService = {
			get: subcategoriesService.getSubcategoryById
		}

		list.remove = function(id){
			list.deleteProcess = true;
			categoriesService.remove(id).then(function(data){
				messageBoxService.showSuccess('Categoría eliminada correctamente.');
				list.deleteProcess = false;
			})
		};

	};

	var categoryEditorController = function($state, category, subcategories, categoriesService, subcategoriesService, messageBoxService, auxFunctions) {
		var editor = this;
		category ? editor.category = category : $state.go('app.lc.categories.list');

		editor.subcategoriesService = {
			get: subcategoriesService.getSubcategoryById
		}
		
		editor.save = function(category){
			editor.loading = true;
			var newCategory = angular.copy(category)
			categoriesService.categoryCreate(newCategory).then(function(data) {
				if(data){
					messageBoxService.showSuccess('Categoría creada correctamente.');
				}else{
					messageBoxService.showError('Ocurrió un error inesperado.');
				}
				$state.go('app.lc.categories.list')
				editor.loading = false;
			})
		}

		editor.update = function(category){
			editor.loading = true;
			var newCategory = angular.copy(category)
			categoriesService.categoryUpdate(newCategory).then(function(data) {
				if(data){
					messageBoxService.showSuccess('Categoría actualizada correctamente.');
				}else{
					messageBoxService.showError('Ocurrió un error inesperado.');
				}
				$state.go('app.lc.categories.list')
				editor.loading = false;
			})
		}

		function init() {
			editor.subcategories = subcategories;

			if(!editor.category.subcategories){
				editor.category.subcategories = [];
			}
		};

		init();
	};

	var categoryImportController = function(){
		var importC = this;
	}

	angular.module('categories')
		.controller('categoriesListController', categoriesListController)
		.controller('categoryCreationCtrl', categoryEditorController)
		.controller('categoryEditorCtrl', categoryEditorController)
		.controller('categoryImportController', categoryImportController)
})();
