(function(){
	'use strict'
	angular.module('categories', [])

	.config(function ($stateProvider, $urlRouterProvider) {

		$stateProvider

			.state('app.lc.categories', {
				url: '/categorias',
				abstract : true,
				template: '<ui-view/>'
			})

			.state('app.lc.categories.list', {
				url: '/lista',
				templateUrl: '/scripts/modules/categories/views/list.html',
				controller: 'categoriesListController as list',
				resolve : {
					categoriesList : function(categoriesService) {
						return categoriesService.retrieveAll()
					}
				}
			})

			.state('app.lc.categories.editor', {
				url: '/editar/:categoryId',
				templateUrl: '/scripts/modules/categories/views/category-editor.html',
				controller: 'categoryEditorCtrl as editor',
				resolve : {
					category : function(categoriesService, $stateParams) {
						var categoryId = $stateParams.categoryId;
						return categoriesService.getCategoryById(categoryId)
					},
					subcategories : function(subcategoriesService) {
						return subcategoriesService.retrieveAll()
					}
				}
			})

			.state('app.lc.categories.create', {
				url: '/crear',
				templateUrl: '/scripts/modules/categories/views/category-editor.html',
				controller: 'categoryCreationCtrl as editor',
				resolve: {
					category : function() {
						return {}
					},
					subcategories : function(subcategoriesService) {
						return subcategoriesService.retrieveAll()
					}
				}
			})

			.state('app.lc.categories.import', {
				url: '/importar',
				templateUrl: '/scripts/modules/categories/views/import.html'
			})
		})
})()
