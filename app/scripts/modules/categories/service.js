(function(){

	'use strict'

	var categoriesService = function(fireRef, Kutral, $q, $firebaseArray, $firebaseObject, auxFunctions) {
		var service = this;

		var kutral = new Kutral(fireRef);
		var categorySchema;

		var categoryModel;

		categorySchema = new Kutral.Schema({
			'title': {type: String, indexOn: true}
		});

		categoryModel = kutral.model('categories', categorySchema);

		service.retrieveAll = function(){
			var categoryRetriveAllPromise = $q.defer();

			var ref = new Firebase(fireRef + 'categories');
			categoryRetriveAllPromise.resolve($firebaseArray(ref));

			return categoryRetriveAllPromise.promise;
		}

		service.getCategoryById = function(id){
			var getCategoryByIdPromise = $q.defer();
			
			categoryModel.find({$id:id}, function(data){
				getCategoryByIdPromise.resolve(data);
			});

			return getCategoryByIdPromise.promise;
		}

		service.getCategoryByTitle = function(title){
			var getCategoryByTitlePromise = $q.defer(),
				ref = $firebaseArray(new Firebase(fireRef + 'categories')),
				categoryFinded;

			ref.$loaded(function(data) {
				data.forEach(function(category){
					if(category.title.toLowerCase() == title.toLowerCase()){
						categoryFinded = category;
						return;
					}
				})

				getCategoryByTitlePromise.resolve(categoryFinded);
				
			});

			return getCategoryByTitlePromise.promise;
		}

		service.categoryCreate = function(category){
			var categoryCreatePromise = $q.defer(),
				ref = $firebaseArray(new Firebase(fireRef + 'categories'));
				
			category.title = category.title.toUpperCase();
			if(category.subcategories){
				category.subcategories = service.subcategoriesToArray(category.subcategories)
			}
			ref.$add(category).then(function(data) {
				categoryCreatePromise.resolve(data.key());
			}, function(error) {
				console.log("Error:", error);
			});

			return categoryCreatePromise.promise;
		}

		service.categoryUpdate = function(category){
			var categoryUpdatePromise = $q.defer();
			category.subcategories = service.subcategoriesToArray(category.subcategories);
			categoryModel.data = category;
			categoryModel.update(function(success) {
				if(success){
					category = categoryModel.data;
				}
				categoryUpdatePromise.resolve(categoryModel.data);
			})

			return categoryUpdatePromise.promise;
		}

		service.remove = function(id){
			var categoryRemovePromise = $q.defer();

			categoryModel.remove({$id: id},function(success) {
				categoryRemovePromise.resolve();
			})

			return categoryRemovePromise.promise;
		}

		service.subcategoriesToArray = function(category) {
			var subcategorie = [], subcategories = [], id;
			category.forEach(function(data) {
				if(data){
					if(data.$id || data.id){
						!data.$id ? id = data.id : id = data.$id;
						subcategorie.push(id)
					}else{
						subcategorie.push(data)
					}
				}
			});
			
			subcategorie.forEach(function(data) {
				subcategories.push(data)
			});

			/*Remove Duplicates*/
			subcategories = subcategories.filter(function(item, pos, self) {
				return self.indexOf(item) == pos;
			})

			return subcategories;
		}
	}

	angular.module('categories')
		.service('categoriesService', categoriesService)
})();
