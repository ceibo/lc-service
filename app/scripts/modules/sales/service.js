(function(){

	'use strict'

	var salesService = function($http, $resource, fireRef, utils, Kutral, $q, $firebaseArray) {
		var service = this;

		var kutral = new Kutral(fireRef);
		var saleSchema;

		var saleModel;
		var successSales;
		var pendingSales;
		var cancelledSales;

		var settingModel;
		var mpCredential;

		saleSchema = new Kutral.Schema({
			'user': {type: 'ObjectId', ref:'users', indexOn: true}
		});

		saleModel = kutral.model('sales', saleSchema);
		successSales = kutral.model.discriminator('success', saleModel);
		pendingSales = kutral.model.discriminator('pending', saleModel);
		cancelledSales = kutral.model.discriminator('cancelled', saleModel);

		settingModel = kutral.model('settings', {});
		mpCredential = kutral.model.discriminator('mpCredential', settingModel);

		service.retrieveMpAll = function() {
            return $http.get(utils + '/payments').then(function(data) {
            	return data.data.response.results;
            });
		};

		service.getMp = function(saleId) {
            return $http.get(utils + '/payments/' + saleId).then(function(data) {
            	return data.data.response.results;
            });
		};

		service.saleMpCreate = function(sale) {
            return $http.post(utils + '/preferences', sale);
		    /*
		    var preference = {
		        "items": [
		            {
		                "title": "Test Modified",
		                "quantity": 1,
		                "currency_id": "ARS",
		                "unit_price": 20.4
		            }
		        ]
		    };
		    */
		};

        service.mpCredentialGet = function() {
            var credentialGetPromise = $q.defer();

			var ref = $firebaseArray(new Firebase(fireRef + 'settings/mpCredential'));

			ref.$loaded(function(data) {
				credentialGetPromise.resolve(data);
			});

            return credentialGetPromise.promise;
        };

        service.mpCredencialCreate = function(credential){
            var credentialCreatePromise = $q.defer(),
                ref = $firebaseArray(new Firebase(fireRef + 'settings/mpCredential'));

            ref.$add(credential).then(function(data) {
                credentialCreatePromise.resolve(data.key());
            }, function(error) {
                console.log('Error:', error);
            });

            return credentialCreatePromise.promise;
        };

		service.mpCredentialUpdate = function(credential){
			var credentialUpdatePromise = $q.defer();

			mpCredential.data = {
				id: credential.$id,
				identif: credential.identif,
				secret: credential.secret
			};

			mpCredential.update(function(success) {
				if(success){
					credential = mpCredential.data;
				}
				credentialUpdatePromise.resolve(mpCredential.data);
			})

			return credentialUpdatePromise.promise;
		};

		service.retrieveAll = function(){
			var saleRetriveAllPromise = $q.defer();
			var pendingsPromise = $q.defer();
			var successPromise = $q.defer();
			var cancelledPromise = $q.defer();

			var refPenging = $firebaseArray(new Firebase(fireRef + 'sales/pending'));
			var refSuccess = $firebaseArray(new Firebase(fireRef + 'sales/success'));
			var refCancelled = $firebaseArray(new Firebase(fireRef + 'sales/cancelled'));

			refPenging.$loaded(function(data) {
				pendingsPromise.resolve(data);
			});

			refSuccess.$loaded(function(data) {
				successPromise.resolve(data);
			});

			refCancelled.$loaded(function(data) {
				cancelledPromise.resolve(data);
			});

			$q.all([pendingsPromise.promise, successPromise.promise, cancelledPromise.promise]).then(function(results) {
				var resultsList = [];

				if(results[0] && results[0].length) {
					resultsList= resultsList.concat(results[0]);
				}

				if(results[1] && results[1].length) {
					resultsList = resultsList.concat(results[1]);
				}

				if(results[2] && results[2].length) {
					resultsList = resultsList.concat(results[2]);
				}
				saleRetriveAllPromise.resolve(resultsList);

			})

			return saleRetriveAllPromise.promise;
		}

		service.retrieveSuccess = function(){
			var retrieveSuccessPromise = $q.defer();

			var refSuccess = $firebaseArray(new Firebase(fireRef + 'sales/success'));

			refSuccess.$loaded(function(data) {
				retrieveSuccessPromise.resolve(data);
			});

			/*successSales.find().populate("user").$asArray(function(data){
				retrieveSuccessPromise.resolve(data);
			});*/

			return retrieveSuccessPromise.promise;
		}

		service.getSaleById = function(id,state){
			var saleGetSaleByIdPromise = $q.defer();
			if(state == 'success'){
				successSales.find({$id:id}).populate("user", function(data){
					saleGetSaleByIdPromise.resolve(data);
				});
			}
			if(state == 'pending'){
				pendingSales.find({$id:id}).populate("user", function(data){
					saleGetSaleByIdPromise.resolve(data);
				});
			}

			if(state == 'cancelled'){
				cancelledSales.find({$id:id}).populate("user", function(data){
					saleGetSaleByIdPromise.resolve(data);
				});
			}
			return saleGetSaleByIdPromise.promise;
		}

		service.saleCreate = function(sale){
			var saleCreatePromise = $q.defer();

			successSales.data = sale;
			successSales.create(function(success) {
				if(success){
					sale = successSales.data;
				}
				saleCreatePromise.resolve(successSales.data);
			})
			return saleCreatePromise.promise;
		}

		service.saleUpdate = function(sale){
			var saleUpdatePromise = $q.defer();

			for (var i = 0; i < sale.items.length; i++) {
				delete sale.items[i]["$$hashKey"];
			};

			if(sale.status == 'pending'){
				pendingSales.data = sale;
				pendingSales.update(function(success) {
					if(success){
						sale = pendingSales.data;
					}
					saleUpdatePromise.resolve(pendingSales.data);
				})
			}else{
				successSales.data = sale;
				successSales.update(function(success) {
					if(success){
						sale = successSales.data;
					}
					saleUpdatePromise.resolve(successSales.data);
				})
			}

			return saleUpdatePromise.promise;
		}

		service.saleChangeState = function(sale){
			var saleChangeStatePromise = $q.defer();

			for (var i = 0; i < sale.items.length; i++) {
				delete sale.items[i]["$$hashKey"];
			};

			if(sale.status == 'pending'){
				sale.status = 'success';
				successSales.data = sale;
				successSales.update(function(success) {
					service.removeSale(sale.id, sale.status).then(function(){
						saleChangeStatePromise.resolve(success)
					})
				})
			}else{
				sale.status = 'pending';
				pendingSales.data = sale;
				pendingSales.update(function(success) {
					service.removeSale(sale.id, sale.status).then(function(){
						saleChangeStatePromise.resolve(success)
					})
				})
			}

			return saleChangeStatePromise.promise;
		}

		service.saleDelete = function(sale){
			var saleDeletePromise = $q.defer();

			if(sale.status == 'success'){
				successSales.remove({$id:sale.id}, function(success) {
					saleDeletePromise.resolve(success);
				})
			}

			if(sale.status == 'pending'){
				pendingSales.remove({$id:sale.id}, function(success) {
					saleDeletePromise.resolve(success);
				})
			}

			if(sale.status == 'cancelled'){
				cancelledSales.remove({$id:sale.id}, function(success) {
					saleDeletePromise.resolve(success);
				})
			}

			return saleDeletePromise.promise
		}

		service.cancelledSale = function(sale){
			var cancelledSalePromise = $q.defer();

			for (var i = 0; i < sale.items.length; i++) {
				delete sale.items[i]["$$hashKey"];
			};

			if(sale.status == 'success'){
				sale.status = 'cancelled';
				cancelledSales.data = sale;
				cancelledSales.update(function(success) {
					successSales.remove({$id:sale.id}, function(success) {
						cancelledSalePromise.resolve(success);
					})
				})
			}else{
				sale.status = 'cancelled';
				cancelledSales.data = sale;
				console.log(cancelledSales.data)
				cancelledSales.update(function(success) {
					pendingSales.remove({$id:sale.id}, function(success) {
						cancelledSalePromise.resolve(success);
					})
				})
			}

			return cancelledSalePromise.promise
		}

		service.removeSale = function(id, status){
			var saleRemovePromise = $q.defer();

			if(status == 'success'){
				pendingSales.remove({$id:id}, function(success) {
					saleRemovePromise.resolve(success);
				})
			}

			if(status == 'pending'){
				successSales.remove({$id:id}, function(success) {
					saleRemovePromise.resolve(success);
				})
			}
			return saleRemovePromise.promise
		}

	}

	angular.module('sales')
		.service('salesService', salesService)
})();
