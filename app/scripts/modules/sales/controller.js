(function(){

	'use strict';

	var salesListController = function(salesList, mpSalesList) {
		var list = this;
		list.rowCollection = salesList;
        list.mpRowCollection = mpSalesList;

        for (var item in list.mpRowCollection) {
            var newDate = moment(list.mpRowCollection[item].collection.date_created).calendar();
            list.mpRowCollection[item].date = {}
            list.mpRowCollection[item].tipo = 'mercadopago';
            list.mpRowCollection[item].status = list.mpRowCollection[item].collection.status;
            list.mpRowCollection[item].date.calendar = newDate;
        }

        if (list.mpRowCollection.length) {
    		list.sales = [].concat(list.rowCollection, list.mpRowCollection);
        } else {
            list.sales = [].concat(list.rowCollection);
        }

        for (var item in list.sales) {
            if (list.sales[item].date) {
                list.sales[item].date.full = moment(list.sales[item].date.calendar, 'DD/MM/YYYY').format();
            }
        }

		function init() {

		}

		init();
	};

	var salesStatsController = function($scope, sales, momentFunctions, auxFunctions) {
		var stat = this;
		stat.sales = sales;

		stat.success = 0;
		stat.pending = 0;
		stat.cancelled = 0;

		stat.salespending = [];

		stat.selectDate = [{'value':'Ventas de hoy'},{'value':'Ventas de la semana'},{'value':'Ventas del mes'}, {'value':'Ventas del año'} , {'value':'Todas las ventas'}];

		stat.setFilter = function(){
			stat.success = 0;
			stat.pending = 0;
			stat.cancelled = 0;
			stat.sales.forEach(function(sale){
				if(sale.status === 'success'){
					if(stat.selectedDate.value === 'Ventas de hoy' && sale.date.day === moment().get('date') && sale.date.month === moment().get('month') && sale.date.year === moment().get('year')){
						stat.success += 1;
					}
					if(stat.selectedDate.value === 'Ventas de la semana' && sale.date.week === moment().get('week') && sale.date.month === moment().get('month') && sale.date.year === moment().get('year')){
						stat.success += 1;
					}
					if(stat.selectedDate.value === 'Ventas del mes' && sale.date.month === moment().get('month') && sale.date.year === moment().get('year')){
						stat.success += 1;
					}
					if(stat.selectedDate.value === 'Ventas del año' && sale.date.year === moment().get('year')){
						stat.success += 1;
					}
					if(stat.selectedDate.value === 'Todas las ventas'){
						stat.success += 1;
					}
				}
				if(sale.status === 'pending'){
					if(stat.selectedDate.value === 'Ventas de hoy' && sale.date.day === moment().get('date') && sale.date.month === moment().get('month') && sale.date.year === moment().get('year')){
						stat.pending += 1;
					}
					if(stat.selectedDate.value === 'Ventas de la semana' && sale.date.week === moment().get('week') && sale.date.month === moment().get('month') && sale.date.year === moment().get('year')){
						stat.pending += 1;
					}
					if(stat.selectedDate.value === 'Ventas del mes' && sale.date.month === moment().get('month') && sale.date.year === moment().get('year')){
						stat.pending += 1;
					}
					if(stat.selectedDate.value === 'Ventas del año' && sale.date.year === moment().get('year')){
						stat.pending += 1;
					}
					if(stat.selectedDate.value === 'Todas las ventas'){
						stat.pending += 1;
					}
				}
				if(sale.status === 'cancelled'){
					if(stat.selectedDate.value === 'Ventas de hoy' && sale.date.day === moment().get('date') && sale.date.month === moment().get('month') && sale.date.year === moment().get('year')){
						stat.cancelled += 1;
					}
					if(stat.selectedDate.value === 'Ventas de la semana' && sale.date.week === moment().get('week') && sale.date.month === moment().get('month') && sale.date.year === moment().get('year')){
						stat.cancelled += 1;
					}
					if(stat.selectedDate.value === 'Ventas del mes' && sale.date.month === moment().get('month') && sale.date.year === moment().get('year')){
						stat.cancelled += 1;
					}
					if(stat.selectedDate.value === 'Ventas del año' && sale.date.year === moment().get('year')){
						stat.cancelled += 1;
					}
					if(stat.selectedDate.value === 'Todas las ventas'){
						stat.cancelled += 1;
					}
				}
			});

			stat.data = [
				{
					key: 'Concretadas',
					y: stat.success,
				},
				{
					key: 'Pendientes',
					y: stat.pending,
				},
				{
					key: 'Canceladas',
					y: stat.cancelled,
				}
			];
		};
		stat.selectedDate = {'value': 'Todas las ventas'};
		stat.setFilter();

		stat.options = {
			chart: {
				type: 'pieChart',
				height: 500,
				x: function(d){return d.key;},
				y: function(d){return d.y;},
				showLabels: true,
				transitionDuration: 500,
				labelThreshold: 0.01,
				legend: {
					margin: {
						top: 5,
						right: 35,
						bottom: 5,
						left: 0
					}
				},
			}
		};

		stat.data = [
			{
				key: 'Concretadas',
				y: stat.success,
			},
			{
				key: 'Pendientes',
				y: stat.pending,
			},
			{
				key: 'Canceladas',
				y: stat.cancelled,
			}
		];

		stat.options2 = {
            chart: {
                type: 'stackedAreaChart',
                height: 350,
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 60,
                    left: 40
                },
                x: function(d){return d[0];},
                y: function(d){return d[1];},
                useVoronoi: true,
                clipEdge: true,
                transitionDuration: 500,
                useInteractiveGuideline: true,
                xAxis: {
                    showMaxMin: true,
                    tickFormat: function(d) {
                        return d3.time.format('%d/%m/%Y')(new Date(d));
                    }
                },
                yAxis: {
                    tickFormat: function(d){
                        return d3.format(',.2f')(d);
                    }
                }
            }
        };

		stat.successData = [];
		stat.pendingData = [];
		stat.cancelledData = [];
		sales.forEach(function(sale){
			if(sale.status === 'success'){
				stat.successData.push([sale.date.dateInMilliseconds, 1]);
				stat.pendingData.push([sale.date.dateInMilliseconds, 0]);
				stat.cancelledData.push([sale.date.dateInMilliseconds, 0]);
			}
			if(sale.status === 'pending'){
				stat.successData.push([sale.date.dateInMilliseconds, 0]);
				stat.pendingData.push([sale.date.dateInMilliseconds, 1]);
				stat.cancelledData.push([sale.date.dateInMilliseconds, 0]);
			}
			if(sale.status === 'cancelled'){
				stat.successData.push([sale.date.dateInMilliseconds, 0]);
				stat.pendingData.push([sale.date.dateInMilliseconds, 0]);
				stat.cancelledData.push([sale.date.dateInMilliseconds, 1]);
			}
		});

        stat.data2 = [
            {
                'key' : 'Concretadas' ,
                //'values' : [ [ 1025409600000 , 23.041422681023] , [ 1028088000000 , 19.854291255832] , [ 1030766400000 , 21.02286281168] , [ 1033358400000 , 22.093608385173] , [ 1036040400000 , 25.108079299458] , [ 1038632400000 , 26.982389242348] , [ 1041310800000 , 19.828984957662] , [ 1043989200000 , 19.914055036294] , [ 1046408400000 , 19.436150539916] , [ 1049086800000 , 21.558650338602] , [ 1051675200000 , 24.395594061773] , [ 1054353600000 , 24.747089309384] , [ 1056945600000 , 23.491755498807] , [ 1059624000000 , 23.376634878164] , [ 1062302400000 , 24.581223154533] , [ 1064894400000 , 24.922476843538] , [ 1067576400000 , 27.357712939042] , [ 1070168400000 , 26.503020572593] , [ 1072846800000 , 26.658901244878] , [ 1075525200000 , 27.065704156445] , [ 1078030800000 , 28.735320452588] , [ 1080709200000 , 31.572277846319] , [ 1083297600000 , 30.932161503638] , [ 1085976000000 , 31.627029785554] , [ 1088568000000 , 28.728743674232] , [ 1091246400000 , 26.858365172675] , [ 1093924800000 , 27.279922830032] , [ 1096516800000 , 34.408301211324] , [ 1099195200000 , 34.794362930439] , [ 1101790800000 , 35.609978198951] , [ 1104469200000 , 33.574394968037] , [ 1107147600000 , 31.979405070598] , [ 1109566800000 , 31.19009040297] , [ 1112245200000 , 31.083933968994] , [ 1114833600000 , 29.668971113185] , [ 1117512000000 , 31.490638014379] , [ 1120104000000 , 31.818617451128] , [ 1122782400000 , 32.960314008183] , [ 1125460800000 , 31.313383196209] , [ 1128052800000 , 33.125486081852] , [ 1130734800000 , 32.791805509149] , [ 1133326800000 , 33.506038030366] , [ 1136005200000 , 26.96501697216] , [ 1138683600000 , 27.38478809681] , [ 1141102800000 , 27.371377218209] , [ 1143781200000 , 26.309915460827] , [ 1146369600000 , 26.425199957518] , [ 1149048000000 , 26.823411519396] , [ 1151640000000 , 23.850443591587] , [ 1154318400000 , 23.158355444054] , [ 1156996800000 , 22.998689393695] , [ 1159588800000 , 27.9771285113] , [ 1162270800000 , 29.073672469719] , [ 1164862800000 , 28.587640408904] , [ 1167541200000 , 22.788453687637] , [ 1170219600000 , 22.429199073597] , [ 1172638800000 , 22.324103271052] , [ 1175313600000 , 17.558388444187] , [ 1177905600000 , 16.769518096208] , [ 1180584000000 , 16.214738201301] , [ 1183176000000 , 18.729632971229] , [ 1185854400000 , 18.814523318847] , [ 1188532800000 , 19.789986451358] , [ 1191124800000 , 17.070049054933] , [ 1193803200000 , 16.121349575716] , [ 1196398800000 , 15.141659430091] , [ 1199077200000 , 17.175388025297] , [ 1201755600000 , 17.286592443522] , [ 1204261200000 , 16.323141626568] , [ 1206936000000 , 19.231263773952] , [ 1209528000000 , 18.446256391095] , [ 1212206400000 , 17.822632399764] , [ 1214798400000 , 15.53936647598] , [ 1217476800000 , 15.255131790217] , [ 1220155200000 , 15.660963922592] , [ 1222747200000 , 13.254482273698] , [ 1225425600000 , 11.920796202299] , [ 1228021200000 , 12.122809090924] , [ 1230699600000 , 15.691026271393] , [ 1233378000000 , 14.720881635107] , [ 1235797200000 , 15.387939360044] , [ 1238472000000 , 13.765436672228] , [ 1241064000000 , 14.631445864799] , [ 1243742400000 , 14.292446536221] , [ 1246334400000 , 16.170071367017] , [ 1249012800000 , 15.948135554337] , [ 1251691200000 , 16.612872685134] , [ 1254283200000 , 18.778338719091] , [ 1256961600000 , 16.756026065421] , [ 1259557200000 , 19.385804443146] , [ 1262235600000 , 22.950590240168] , [ 1264914000000 , 23.61159018141] , [ 1267333200000 , 25.708586989581] , [ 1270008000000 , 26.883915999885] , [ 1272600000000 , 25.893486687065] , [ 1275278400000 , 24.678914263176] , [ 1277870400000 , 25.937275793024] , [ 1280548800000 , 29.461381693838] , [ 1283227200000 , 27.357322961861] , [ 1285819200000 , 29.057235285673] , [ 1288497600000 , 28.549434189386] , [ 1291093200000 , 28.506352379724] , [ 1293771600000 , 29.449241421598] , [ 1296450000000 , 25.796838168807] , [ 1298869200000 , 28.740145449188] , [ 1301544000000 , 22.091744141872] , [ 1304136000000 , 25.07966254541] , [ 1306814400000 , 23.674906973064] , [ 1309406400000 , 23.418002742929] , [ 1312084800000 , 23.24364413887] , [ 1314763200000 , 31.591854066817] , [ 1317355200000 , 31.497112374114] , [ 1320033600000 , 26.67238082043] , [ 1322629200000 , 27.297080015495] , [ 1325307600000 , 20.174315530051] , [ 1327986000000 , 19.631084213898] , [ 1330491600000 , 20.366462219461] , [ 1333166400000 , 19.284784434185] , [ 1335758400000 , 19.157810257624]]
            	'values' : stat.successData
            },

            {
                'key' : 'Pendientes' ,
                //'values' : [ [ 1025409600000 , 7.9356392949025] , [ 1028088000000 , 7.4514668527298] , [ 1030766400000 , 7.9085410566608] , [ 1033358400000 , 5.8996782364764] , [ 1036040400000 , 6.0591869346923] , [ 1038632400000 , 5.9667815800451] , [ 1041310800000 , 8.65528925664] , [ 1043989200000 , 8.7690763386254] , [ 1046408400000 , 8.6386160387453] , [ 1049086800000 , 5.9895557449743] , [ 1051675200000 , 6.3840324338159] , [ 1054353600000 , 6.5196511461441] , [ 1056945600000 , 7.0738618553114] , [ 1059624000000 , 6.5745957367133] , [ 1062302400000 , 6.4658359184444] , [ 1064894400000 , 2.7622758754954] , [ 1067576400000 , 2.9794782986241] , [ 1070168400000 , 2.8735432712019] , [ 1072846800000 , 1.6344817513645] , [ 1075525200000 , 1.5869248754883] , [ 1078030800000 , 1.7172279157246] , [ 1080709200000 , 1.9649927409867] , [ 1083297600000 , 2.0261695079196] , [ 1085976000000 , 2.0541261923929] , [ 1088568000000 , 3.9466318927569] , [ 1091246400000 , 3.7826770946089] , [ 1093924800000 , 3.9543021004028] , [ 1096516800000 , 3.8309891064711] , [ 1099195200000 , 3.6340958946166] , [ 1101790800000 , 3.5289755762525] , [ 1104469200000 , 5.702378559857] , [ 1107147600000 , 5.6539569019223] , [ 1109566800000 , 5.5449506370392] , [ 1112245200000 , 4.7579993280677] , [ 1114833600000 , 4.4816139372906] , [ 1117512000000 , 4.5965558568606] , [ 1120104000000 , 4.3747066116976] , [ 1122782400000 , 4.4588822917087] , [ 1125460800000 , 4.4460351848286] , [ 1128052800000 , 3.7989113035136] , [ 1130734800000 , 3.7743883140088] , [ 1133326800000 , 3.7727852823828] , [ 1136005200000 , 7.2968111448895] , [ 1138683600000 , 7.2800122043237] , [ 1141102800000 , 7.1187787503354] , [ 1143781200000 , 8.351887016482] , [ 1146369600000 , 8.4156698763993] , [ 1149048000000 , 8.1673298604231] , [ 1151640000000 , 5.5132447126042] , [ 1154318400000 , 6.1152537710599] , [ 1156996800000 , 6.076765091942] , [ 1159588800000 , 4.6304473798646] , [ 1162270800000 , 4.6301068469402] , [ 1164862800000 , 4.3466656309389] , [ 1167541200000 , 6.830104897003] , [ 1170219600000 , 7.241633040029] , [ 1172638800000 , 7.1432372054153] , [ 1175313600000 , 10.608942063374] , [ 1177905600000 , 10.914964549494] , [ 1180584000000 , 10.933223880565] , [ 1183176000000 , 8.3457524851265] , [ 1185854400000 , 8.1078413081882] , [ 1188532800000 , 8.2697185922474] , [ 1191124800000 , 8.4742436475968] , [ 1193803200000 , 8.4994601179319] , [ 1196398800000 , 8.7387319683243] , [ 1199077200000 , 6.8829183612895] , [ 1201755600000 , 6.984133637885] , [ 1204261200000 , 7.0860136043287] , [ 1206936000000 , 4.3961787956053] , [ 1209528000000 , 3.8699674365231] , [ 1212206400000 , 3.6928925238305] , [ 1214798400000 , 6.7571718894253] , [ 1217476800000 , 6.4367313362344] , [ 1220155200000 , 6.4048441521454] , [ 1222747200000 , 5.4643833239669] , [ 1225425600000 , 5.3150786833374] , [ 1228021200000 , 5.3011272612576] , [ 1230699600000 , 4.1203601430809] , [ 1233378000000 , 4.0881783200525] , [ 1235797200000 , 4.1928665957189] , [ 1238472000000 , 7.0249415663205] , [ 1241064000000 , 7.006530880769] , [ 1243742400000 , 6.994835633224] , [ 1246334400000 , 6.1220222336254] , [ 1249012800000 , 6.1177436137653] , [ 1251691200000 , 6.1413396231981] , [ 1254283200000 , 4.8046006145874] , [ 1256961600000 , 4.6647600660544] , [ 1259557200000 , 4.544865006255] , [ 1262235600000 , 6.0488249316539] , [ 1264914000000 , 6.3188669540206] , [ 1267333200000 , 6.5873958262306] , [ 1270008000000 , 6.2281189839578] , [ 1272600000000 , 5.8948915746059] , [ 1275278400000 , 5.5967320482214] , [ 1277870400000 , 0.99784432084837] , [ 1280548800000 , 1.0950794175359] , [ 1283227200000 , 0.94479734407491] , [ 1285819200000 , 1.222093988688] , [ 1288497600000 , 1.335093106856] , [ 1291093200000 , 1.3302565104985] , [ 1293771600000 , 1.340824670897] , [ 1296450000000 , 0] , [ 1298869200000 , 0] , [ 1301544000000 , 0] , [ 1304136000000 , 0] , [ 1306814400000 , 0] , [ 1309406400000 , 0] , [ 1312084800000 , 0] , [ 1314763200000 , 0] , [ 1317355200000 , 4.4583692315] , [ 1320033600000 , 3.6493043348059] , [ 1322629200000 , 3.8610064091761] , [ 1325307600000 , 5.5144800685202] , [ 1327986000000 , 5.1750695220791] , [ 1330491600000 , 5.6710066952691] , [ 1333166400000 , 5.5611890039181] , [ 1335758400000 , 5.5979368839939]]
				'values' : stat.pendingData
            },

			{
                'key' : 'Canceladas' ,
                //'values' : [ [ 1025409600000 , 7.9356392949025] , [ 1028088000000 , 7.4514668527298] , [ 1030766400000 , 7.9085410566608] , [ 1033358400000 , 5.8996782364764] , [ 1036040400000 , 6.0591869346923] , [ 1038632400000 , 5.9667815800451] , [ 1041310800000 , 8.65528925664] , [ 1043989200000 , 8.7690763386254] , [ 1046408400000 , 8.6386160387453] , [ 1049086800000 , 5.9895557449743] , [ 1051675200000 , 6.3840324338159] , [ 1054353600000 , 6.5196511461441] , [ 1056945600000 , 7.0738618553114] , [ 1059624000000 , 6.5745957367133] , [ 1062302400000 , 6.4658359184444] , [ 1064894400000 , 2.7622758754954] , [ 1067576400000 , 2.9794782986241] , [ 1070168400000 , 2.8735432712019] , [ 1072846800000 , 1.6344817513645] , [ 1075525200000 , 1.5869248754883] , [ 1078030800000 , 1.7172279157246] , [ 1080709200000 , 1.9649927409867] , [ 1083297600000 , 2.0261695079196] , [ 1085976000000 , 2.0541261923929] , [ 1088568000000 , 3.9466318927569] , [ 1091246400000 , 3.7826770946089] , [ 1093924800000 , 3.9543021004028] , [ 1096516800000 , 3.8309891064711] , [ 1099195200000 , 3.6340958946166] , [ 1101790800000 , 3.5289755762525] , [ 1104469200000 , 5.702378559857] , [ 1107147600000 , 5.6539569019223] , [ 1109566800000 , 5.5449506370392] , [ 1112245200000 , 4.7579993280677] , [ 1114833600000 , 4.4816139372906] , [ 1117512000000 , 4.5965558568606] , [ 1120104000000 , 4.3747066116976] , [ 1122782400000 , 4.4588822917087] , [ 1125460800000 , 4.4460351848286] , [ 1128052800000 , 3.7989113035136] , [ 1130734800000 , 3.7743883140088] , [ 1133326800000 , 3.7727852823828] , [ 1136005200000 , 7.2968111448895] , [ 1138683600000 , 7.2800122043237] , [ 1141102800000 , 7.1187787503354] , [ 1143781200000 , 8.351887016482] , [ 1146369600000 , 8.4156698763993] , [ 1149048000000 , 8.1673298604231] , [ 1151640000000 , 5.5132447126042] , [ 1154318400000 , 6.1152537710599] , [ 1156996800000 , 6.076765091942] , [ 1159588800000 , 4.6304473798646] , [ 1162270800000 , 4.6301068469402] , [ 1164862800000 , 4.3466656309389] , [ 1167541200000 , 6.830104897003] , [ 1170219600000 , 7.241633040029] , [ 1172638800000 , 7.1432372054153] , [ 1175313600000 , 10.608942063374] , [ 1177905600000 , 10.914964549494] , [ 1180584000000 , 10.933223880565] , [ 1183176000000 , 8.3457524851265] , [ 1185854400000 , 8.1078413081882] , [ 1188532800000 , 8.2697185922474] , [ 1191124800000 , 8.4742436475968] , [ 1193803200000 , 8.4994601179319] , [ 1196398800000 , 8.7387319683243] , [ 1199077200000 , 6.8829183612895] , [ 1201755600000 , 6.984133637885] , [ 1204261200000 , 7.0860136043287] , [ 1206936000000 , 4.3961787956053] , [ 1209528000000 , 3.8699674365231] , [ 1212206400000 , 3.6928925238305] , [ 1214798400000 , 6.7571718894253] , [ 1217476800000 , 6.4367313362344] , [ 1220155200000 , 6.4048441521454] , [ 1222747200000 , 5.4643833239669] , [ 1225425600000 , 5.3150786833374] , [ 1228021200000 , 5.3011272612576] , [ 1230699600000 , 4.1203601430809] , [ 1233378000000 , 4.0881783200525] , [ 1235797200000 , 4.1928665957189] , [ 1238472000000 , 7.0249415663205] , [ 1241064000000 , 7.006530880769] , [ 1243742400000 , 6.994835633224] , [ 1246334400000 , 6.1220222336254] , [ 1249012800000 , 6.1177436137653] , [ 1251691200000 , 6.1413396231981] , [ 1254283200000 , 4.8046006145874] , [ 1256961600000 , 4.6647600660544] , [ 1259557200000 , 4.544865006255] , [ 1262235600000 , 6.0488249316539] , [ 1264914000000 , 6.3188669540206] , [ 1267333200000 , 6.5873958262306] , [ 1270008000000 , 6.2281189839578] , [ 1272600000000 , 5.8948915746059] , [ 1275278400000 , 5.5967320482214] , [ 1277870400000 , 0.99784432084837] , [ 1280548800000 , 1.0950794175359] , [ 1283227200000 , 0.94479734407491] , [ 1285819200000 , 1.222093988688] , [ 1288497600000 , 1.335093106856] , [ 1291093200000 , 1.3302565104985] , [ 1293771600000 , 1.340824670897] , [ 1296450000000 , 0] , [ 1298869200000 , 0] , [ 1301544000000 , 0] , [ 1304136000000 , 0] , [ 1306814400000 , 0] , [ 1309406400000 , 0] , [ 1312084800000 , 0] , [ 1314763200000 , 0] , [ 1317355200000 , 4.4583692315] , [ 1320033600000 , 3.6493043348059] , [ 1322629200000 , 3.8610064091761] , [ 1325307600000 , 5.5144800685202] , [ 1327986000000 , 5.1750695220791] ]
				'values' : stat.cancelledData
            }
        ];

        console.log(stat.successData);

		function init() {
			stat.selectedDate = stat.selectDate[4];
		}

		init();
	};


	var salesEditorController = function(sale, salesService, productsService, $state, auxFunctions, usersService, messageBoxService, momentFunctions) {
		var editor = this;
		editor.sale = sale;

		if(!sale.user){
			//sale.user = sale.client
		}
		editor.model = {};

		editor.options = {
			formState: {
				horizontalLabelClass: 'col-sm-2',
				horizontalFieldClass: 'col-md-5',
				readOnly: true
			}
		};

		editor.userFields = [
			{
				key: 'firstName',
				type: 'horizontalInputReadOnly',
				templateOptions: {
					label: 'Nombre'
				}
			},
			{
				key: 'lastName',
				type: 'horizontalInputReadOnly',
				templateOptions: {
					label: 'Apellido'
				}
			},
			{
				key: 'email',
				type: 'horizontalInputReadOnly',
				templateOptions: {
					label: 'Email'
				}
			},
			{
				key: 'dni',
				type: 'horizontalInputReadOnly',
				templateOptions: {
					label: 'DNI'
				}
			},
			{
				key: 'street',
				type: 'horizontalInputReadOnly',
				templateOptions: {
					label: 'Dirección'
				}
			},
			{
				key: 'city',
				type: 'horizontalInputReadOnly',
				templateOptions: {
					label: 'Ciudad'
				}
			}
		];

		editor.saleUpdate = function(sale){
			editor.loading = true;
			var newSale = angular.copy(sale);
			if(newSale.user){
				newSale.user = newSale.user.id;
			}

			salesService.saleUpdate(newSale).then(function(data) {
				if(data){
					$state.go($state.current, {}, {reload: true});
					editor.loading = false;
					messageBoxService.showSuccess('Datos actualizados correctamente.');
				}
			});
		};

		editor.saleChangeState = function(sale){
			editor.loading = true;
			var newSale = angular.copy(sale);
			if(newSale.user){
				var user = newSale.user;
				if(!user.sales){
                    user.sales = [];
                }
				newSale.user = newSale.user.id;
			} else {
				var user = null;
			}

			newSale.date = momentFunctions.getDate();

			salesService.saleChangeState(newSale).then(function(data) {
				if(data){
					if(sale.status === 'success'){
						if(user){
							user.sales.push(sale.id);
							//usersService.userUpdate(user).then(function(data) {

							productsService.updateProductStock(sale.items, 'resta').then(function() {
								$state.go('app.lc.sales.list');
								editor.loading = false;
							});

							//})
						}else{
							$state.go('app.lc.sales.list');
							editor.loading = false;
						}

					}else{
						productsService.updateProductStock(sale.items, 'suma').then(function() {
							$state.go('app.lc.sales.list');
							editor.loading = false;
						});
					}
				}

			});
		};

		editor.saleCancelled = function(sale){
			editor.loading = true;
			var newSale = angular.copy(sale);
			var user = newSale.user;
			if(!user.sales){user.sales = [];}

			newSale.date = momentFunctions.getDate();

			newSale.user = newSale.user.id;
			salesService.cancelledSale(newSale).then(function(data) {
				$state.go('app.lc.sales.list', {}, {reload: true});
				editor.deleting = false;
				messageBoxService.showSuccess('Venta cancelada.');
			});
		};

		editor.saleDelete = function(sale){
			editor.deleting = true;
			salesService.saleDelete(sale).then(function(data) {
				if(data){
					$state.go('app.lc.sales.list', {}, {reload: true});
					editor.deleting = false;
					messageBoxService.showSuccess('Venta eliminada correctamente.');
				}
			});
		};

		editor.newField = {};
		editor.editing = false;

		editor.removeElement = function(collection, element){
			auxFunctions.removeElement(collection, element);
		};

		editor.editInline = function(field) {
			editor.editing = editor.sale.items.indexOf(field);
			editor.newField = angular.copy(field);
		};

		editor.edit = function(){
			editor.editMode = !editor.editMode;
		};

		editor.saveField = function(index, sale) {
			if (editor.editing !== false) {
				editor.editing = false;
				editor.edit();
			}
		};

		editor.cancel = function(index) {
			if (editor.editing !== false) {
				editor.sale.items[editor.editing] = editor.newField;
				editor.editing = false;
				editor.edit();
			}
		};

		function init () {
		}

		init();
	};

	var salesCreationController = function($scope, productsService, titles, usersService, salesService, AuthTokenService, momentFunctions, auxFunctions, $state) {
		var create = this;
        create.type = 'white';
		create.model = {};
        create.items = [];
		create.item = {};
        create.item.type = 'white';
        create.titles = titles;
		//create.userExist = false;
		//create.fields = [];

        create.selectType = [
            {'value':'Factura', 'value2':'white'},
            {'value':'Consignación', 'value2':'black'}
        ];

        // table
        create.newField = {};
        create.editing = false;

        create.addElement = function(collection, element){
            auxFunctions.addElement(collection, element);
            create.edit();
            create.editInline(element);
        };

        create.removeElement = function(collection, element){
            auxFunctions.removeElement(collection, element);
        };

        create.editInline = function(field) {
            create.editing = create.items.indexOf(field);
            create.newField = angular.copy(field);
        };

        create.edit = function(){
            create.editMode = !create.editMode;
        };

        create.saveField = function(index, sale) {
            if (create.editing !== false) {
                create.editing = false;
                create.edit();
            }
        };

        create.cancel = function(index) {
            if (create.editing !== false) {
                create.items[create.editing] = create.newField;
                create.editing = false;
                create.edit();
            }
        };

        create.findProduct = function(ean, index){
            if(ean && ean.toString().length === 13){
                productsService.searchByEAN(ean).then(function(data) {
                    if(data){
                        create.items[index] = data;
                        create.findProd = true;

                        create.item.title = data.title;
                        create.item.price = data.price;
                        create.item.stock = data.stock;

                    } else {
                        create.findProd = false;
                    }
                });
            }
        };

        create.findProductByTitle = function(title, index){
            if(title){
                productsService.searchByTitle(title).then(function(data) {
                    if(data){
                        create.items[index] = data;
                        create.findProd = true;

                        create.item.ean = data.ean;
                        create.item.price = data.price;
                        create.item.stock = data.stock;

                    } else {
                        create.findProd = false;
                    }
                });
            }
        };

		create.findUser = function(email){
			if(create.validateEmail(email)){
				usersService.getUserByEmailAsObject(email).then(function(data) {
					if(data){
						//create.userExist = true;
						create.user = data;
                        create.findRegisteredUser = true;
					} else {
                        create.findRegisteredUser = false;
                    }
				});
			}
		};

		create.validateEmail = function(email) {
			var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			return re.test(email);
		};

		create.addItems = function(){
            create.items.push(create.item);
            create.form2.$setUntouched();
            create.item = {};
            create.item.type = 'white';

		};

		create.createSale = function(items, user){
			var sale = [];
			var itemsArray = [];

            for (var i in items) {
                itemsArray.push(items[i]);
            }

			itemsArray.forEach(function(item, index){
				if(Array.isArray(item)){
					itemsArray[index] = auxFunctions.convArrToObj(item);
				}
			});


			if(!user.sales){
				user.sales = [];
			};

			var date = momentFunctions.getDate();

			if(user.id){
				//push sale as success and push sale to user
				sale.push({ 'items': itemsArray, 'user': user.id, 'status': 'success', 'date': date, 'type' : create.type })
				salesService.saleCreate(sale[0]).then(function(data) {
					if(data){
						user.sales.push(data.id)

						usersService.userUpdate(user).then(function(data) {

						})

						//descuento stock
						if(create.findProduct){
							productsService.updateProductStock(itemsArray, 'resta').then(function() {
								$state.go('app.lc.sales.list');
							});
						}
					}
				});
			} else {
				//create user and push sale, push sale as success
				/*if(user.email){
					AuthTokenService.optin(user).then(function(dataCreateUser) {
						console.log(dataCreateUser)
					});
				}else{*/
					//no create user, push sale as success
				sale.push({ 'items': itemsArray, 'client': user, 'status': 'success', 'date': date, 'type' : create.type })
				salesService.saleCreate(sale[0]).then(function(data) {
					if(data){

						//descuento stock
						if(create.findProduct){
							productsService.updateProductStock(itemsArray, 'resta').then(function() {
								$state.go('app.lc.sales.list');
							});
						}
					}
				});
				//}
			}
		};

		create.fields = [
					{
						type: 'horizontalInputFuntions',
						key: 'ean',
						templateOptions: {
							type: 'number',
							label: 'Código de barras',
							required: true,
							min: 0,
							minlength: 13,
							maxlength: 13,
							onChange: function($viewValue, $modelValue, scope) {
								create.findProduct($viewValue);
							}
						}
					},
					{
						type: 'typeahead',
						key: 'title',
						templateOptions: {
                            options: titles,
							type: 'text',
							label: 'Título',
							required: true,
							minlength: 0,
                            onKeyup: function($viewValue, $modelValue, scope) {
                                create.findProductByTitle($viewValue);
                            },
                            onFocus: function($viewValue, $modelValue, scope) {
                                create.findProductByTitle($viewValue);
                            }
						}
					},
					{
						type: 'horizontalInput',
						key: 'price',
						templateOptions: {
							type: 'number',
							label: 'Precio',
							required: true,
							min: 0,
							minlength: 0,
						}
					},
					{
						type: 'horizontalInput',
						key: 'stock',
						templateOptions: {
							type: 'number',
							label: 'Stock',
							min: 0,
							minlength: 0,
						},
						expressionProperties: {
							hide: function($viewValue, $modelValue, scope) {
								//return !create.findProd;
							}
						}
					},
					{
						type: 'horizontalInput',
						key: 'quantity',
						templateOptions: {
							type: 'number',
							label: 'Cantidad',
							required: true,
							min: 0,
							minlength: 0,
						}
					}

		];


		create.fieldsUser = [
					{
						type: 'horizontalInputFuntions',
						key: 'email',
						templateOptions: {
							type: 'email',
							label: 'Email',
							minlength: 0,
							onChange: function($viewValue, $modelValue, scope) {
								create.findUser($viewValue);
							},
                            focus: true
                        }
					},
					{
						type: 'horizontalInput',
						key: 'firstName',
						templateOptions: {
							type: 'text',
							label: 'Nombre',
							required: true,
							maxlength: 20,
							minlength: 0,
						}
					},
					{
						type: 'horizontalInput',
						key: 'lastName',
						templateOptions: {
							type: 'text',
							label: 'Apellido',
							maxlength: 20,
							minlength: 0,
							required: true
						}
					},
					{
						type: 'horizontalInput',
						key: 'dni',
						templateOptions: {
							type: 'text',
							label: 'DNI',
							pattern: '\\d{1,2}.\\d{3}.\\d{3}',
							placeholder: '55.555.55',
							required: true
						}
					},
					{
						type: 'horizontalInput',
						key: 'cuil',
						templateOptions: {
							type: 'text',
							label: 'CUIL',
							pattern: '\\d{1,2}-\\d{7,8}-\\d{1,2}',
							placeholder: '55-55555555-55',
							required: true
						}
					},
					{
						type: 'horizontalInput',
						key: 'phone',
						templateOptions: {
							type: 'number',
							label: 'Teléfono',
							min: '0',
							required: true
						}
					},
					{
						type: 'horizontalInput',
						key: 'city',
						ngModelAttrs: {
							'': {
								'value': 'googleplace'
							},
							'off': {
								'value': 'autocomplete'
							}
						},
						templateOptions: {
							type: 'text',
							label: 'Ciudad',
							required: true
						}
					},
					{
						type: 'horizontalInput',
						key: 'street',
						templateOptions: {
							type: 'text',
							label: 'Calle',
							required: true
						}
					},
					{
						type: 'horizontalInput',
						key: 'zip',
						templateOptions: {
							type: 'number',
							label: 'Código postal',
							min: '0',
							minlength: 4,
							maxlength: 4,
							required: true
						}
					},
					{
						type: 'horizontalSwitch',
						key: 'buyGuns',
						value: false,
						templateOptions: {
							label: 'Armas de fuego'
						}
					},
					{
						type: 'horizontalInput',
						key: 'cluaucc',
						templateOptions: {
							type: 'number',
							label: 'CLUAUCC',
							minlength: 0,
						},
						hideExpression: '!model.buyGuns'
					},
					{
						type: 'horizontalInput',
						key: 'cluauc',
						templateOptions: {
							type: 'number',
							label: 'CLUAUC',
							minlength: 0,
						},
						hideExpression: '!model.buyGuns'
					},
					{
						type: 'horizontalInput',
						key: 'docket',
						templateOptions: {
							type: 'text',
							label: 'Legajo',
							pattern: '3-\\d{1,2}.\\d{3}.\\d{3}',
							placeholder: '3-55.555.55',
							minlength: 0,
							required: true
						},
						hideExpression: '!model.buyGuns || (!model.cluaucc && !model.cluauc)'
					},
					{
						type: 'horizontalInput',
						key: 'tendency',
						templateOptions: {
							type: 'number',
							label: 'Tendencia',
							required: true,
							minlength: 0,
						},
						hideExpression: '!model.buyGuns'
					},
					{
						type: 'horizontalInput',
						key: 'consumerCard',
						templateOptions: {
							type: 'number',
							label: 'Tarjeta de consumo',
							required: true,
							minlength: 0,
						},
						hideExpression: '!model.buyGuns'
					},
					{
						type: 'horizontalInput',
						key: 'cuim',
						templateOptions: {
							type: 'number',
							label: 'CUIM',
							required: true,
							minlength: 0,
						},
						hideExpression: '!model.buyGuns'
					},
					{
						type: 'horizontalInput',
						key: 'serialNumber',
						templateOptions: {
							type: 'number',
							label: 'Número de serie',
							required: true,
							minlength: 0,
						},
						hideExpression: '!model.buyGuns'
					},
					{
						type: 'horizontalTextarea',
						key: 'description',
						templateOptions: {
							type: 'text',
							label: 'Descripción',
							required: false,
							rows: 4
						},
						hideExpression: '!model.buyGuns'
					}
		];

		function init() {
		}

		init();
	};

    var saleMercadopagoController = function(sale) {
        var mp = this;

        mp.sale = sale[0].collection;

        function init() {

        }

        init();
    };

    var salesConfigController = function(salesService, messageBoxService, credential) {
        var config = this;

        config.credential = credential;
        config.loading = false;

        config.create = function(credential) {
            config.loading = true;
            salesService.mpCredencialCreate(credential).then(function() {
                setTimeout(function () { location.reload(); }, 200);
            });
        };

        config.update = function(credential) {
            config.loading = true;
            salesService.mpCredentialUpdate(credential).then(function() {
                config.loading = false;
                messageBoxService.showSuccess('Datos actualizados correctamente.');
            });
        };

        function init() {

        }

        init();
    };

	angular.module('sales')
		.controller('salesListController', salesListController)
		.controller('salesStatsController', salesStatsController)
		.controller('salesCreationController', salesCreationController)
        .controller('salesEditionController', salesEditorController)
		.controller('saleMercadopagoController', saleMercadopagoController)
        .controller('salesConfigController', salesConfigController)
;
})();
