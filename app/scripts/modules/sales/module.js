(function(){
	'use strict'
	angular.module('sales', [])

	.config(function ($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('app.lc.sales', {
				url: '/ventas',
				abstract : true,
				template: '<ui-view/>'
			})

			.state('app.lc.sales.list', {
				url: '/lista',
				templateUrl: '/scripts/modules/sales/views/list.html',
				controller: 'salesListController as list',
				resolve : {
					salesList : function(salesService) {
						return salesService.retrieveAll();
					},
					mpSalesList: function(salesService) {
						return salesService.retrieveMpAll().then(function(data) {
							return data;
						}, function(err) {
							return {};
						});
					}
				}
			})

			.state('app.lc.sales.stats', {
				url: '/estadisticas',
				templateUrl: '/scripts/modules/sales/views/stats.html',
				controller: 'salesStatsController as stat',
				resolve : {
					sales : function(salesService) {
						return salesService.retrieveAll();
					}
				}
			})

			.state('app.lc.sales.create', {
				url: '/crear/',
				templateUrl: '/scripts/modules/sales/views/sale-create.html',
				controller: 'salesCreationController as create',
				resolve: {
					titles : function(productsService) {
						return productsService.retrieveAll().then(function(data) {
							var titles = [];
							for (var i in data) {
								titles.push(data[i].title);
							}
							return titles;
						});
					}
				}
    		})

			.state('app.lc.sales.editor', {
				url: '/editar/:state/:saleId',
				templateUrl: '/scripts/modules/sales/views/sale-editor.html',
				controller: 'salesEditionController as editor',
				resolve : {
					sale : function(salesService, $stateParams) {
						var saleId = $stateParams.saleId;
						var saletState = $stateParams.state;
						return salesService.getSaleById(saleId,saletState);
					}
				}
			})

			.state('app.lc.sales.mercadopago', {
				url: '/venta/mercadopago/:saleId',
				templateUrl: '/scripts/modules/sales/views/sale-mercadopago.html',
				controller: 'saleMercadopagoController as mp',
				resolve : {
					sale : function(salesService, $stateParams) {
						var saleId = $stateParams.saleId;
						return salesService.getMp(saleId).then(function(data) {
							return data;
						}, function(err) {
							return {};
						});
					}
				}
			})

            .state('app.lc.sales.config', {
                url: '/configurar',
                templateUrl: '/scripts/modules/sales/views/sale-config.html',
                controller: 'salesConfigController as config',
                resolve : {
                    credential: function(salesService) {
                        return salesService.mpCredentialGet().then(function(data) {
                            return data[0];
                        }, function(err) {
                            return {};
                        });
                    }
                }
            })

		;
	});
})();
