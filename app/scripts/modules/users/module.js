(function(){
	'use strict';
	angular.module('users', [])

	.config(function ($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('app.lc.users', {
				url: '/usuarios',
				abstract : true,
				template: '<ui-view/>'
			})

			.state('app.lc.users.list', {
				url: '/lista',
				templateUrl: '/scripts/modules/users/views/list.html',
				controller: 'usersListController as list',
				resolve : {
					usersList : function(usersService) {
						return usersService.retrieveAll()
					}
				}
			})

			.state('app.lc.users.create', {
				url: '/crear',
				templateUrl: '/scripts/modules/users/views/user-editor.html',
				controller: 'usersCreationController as editor',
				resolve : {
					user : function() {
						return {}
					}
				}
			})

			.state('app.lc.users.editor', {
				url: '/editar/:userId',
				templateUrl: '/scripts/modules/users/views/user-editor.html',
				controller: 'usersEditionController as editor',
				resolve : {
					user : function(usersService, $stateParams, $state) {
						var userId = $stateParams.userId;
						return usersService.getUserById(userId).then(function(data){
							if(data){
								return data
							}else{
								$state.go('app.lc.users.list')
							}
						})
					}
				}
			})
	})

	.directive('googleplace', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, model) {
				var options = {
					types: [],
					componentRestrictions: {}
				};
				scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

				google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
					scope.$apply(function() {
						model.$setViewValue(element.val());
					});
				});
			}
		};
	})

})()