(function(){

	'use strict';

	var usersService = function(fireRef, Kutral, $q, $firebaseArray, AuthTokenService) {
		var service = this;

		var kutral = new Kutral(fireRef);
		var userSchema;

		var userModel;

		userSchema = new Kutral.Schema({
			'email': {type: String, indexOn: true},
			'sales': {type: 'ObjectId', ref:'sales', indexOn: true}		
		});

		userModel = kutral.model('users', userSchema);


		service.retrieveAll = function(){
			var usersRetriveAllPromise = $q.defer();

			var ref = $firebaseArray(new Firebase(fireRef + 'users'));

			ref.$loaded(function(data) {
				usersRetriveAllPromise.resolve(data);
			});

			return usersRetriveAllPromise.promise;
		}

		service.getTotal = function(){
			var getTotalPromise = $q.defer();

			/*userModel.find().$asArray(function(data){
				getTotalPromise.resolve(data.length)
			});*/

			var ref = $firebaseArray(new Firebase(fireRef + 'users'));

			ref.$loaded(function(data) {
				getTotalPromise.resolve(data.length);
			});
			
			return getTotalPromise.promise;
		}

		service.getUserById = function(id){
			var getUserByIdPromise = $q.defer();
			
			userModel.find({$id:id}, function(data){
				getUserByIdPromise.resolve(data);
			});

			return getUserByIdPromise.promise;
		}

		service.getUserByEmail = function(email){
			var getUserByEmailPromise = $q.defer();
			var email = email.toString();
			userModel.find({email:email}).$asArray(function(data){
				getUserByEmailPromise.resolve(data[0]);
			});

			return getUserByEmailPromise.promise;
		}

		service.getUserByEmailAsObject = function(email){
			var getUserByEmailPromise = $q.defer();
			userModel.find({email:email}, function(data){
				var dataResult = service.getDataFromObject(data);

				getUserByEmailPromise.resolve(dataResult);
			});

			return getUserByEmailPromise.promise;
		}

		service.userCreate = function(user){
			var userCreatePromise = $q.defer();

			AuthTokenService.createUser(user).then(function(data){
				delete user.password
				user.$id = data.uid;
				user.id = data.uid;
				userModel.data = user;
				userModel.save(function(success) {
					if(success){
						user = userModel.data;
					}
					userCreatePromise.resolve(userModel.data);
				})

			}, function(error) {
				userCreatePromise.reject(error);
			});

			return userCreatePromise.promise;
		}

		service.userUpdate = function(user){
			var userUpdatePromise = $q.defer();

			userModel.data = user;
			userModel.update(function(success) {
				if(success){
					user = userModel.data;
				}
				userUpdatePromise.resolve(userModel.data);
			})

			return userUpdatePromise.promise;
		}

		service.userRemove = function(user){
			var userRemovePromise = $q.defer();

			if(user.id){ var id = user.id };
			if(user.$id){ var id = user.$id };

			userModel.data = user;
			userModel.remove({$id: id},function(success){
				if(success){
					user = userModel.data;
				}
				userRemovePromise.resolve(success);
			})

			return userRemovePromise.promise;
		}

		service.getDataFromObject = function(data){
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					if(key.indexOf("$") !== -1){
						//delete data[key]
					}else{
						var dataKey = data[key];
					}
				}
			}

			return dataKey;
		}
	};
	 
	angular
		.module('users')
			.service('usersService', usersService)

})()