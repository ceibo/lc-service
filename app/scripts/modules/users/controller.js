(function(){

	'use strict';

	var usersListController = function(usersList) {
		var list = this;

		function init() {
			list.users = usersList;
		};

		init();
	}

	var usersEditorController = function($state, user, usersService, messageBoxService) {
		var editor = this;
		editor.user = user;

		if(!editor.user.role){
			editor.user.role = 'customer';
		}

		editor.model = {
			
		};

		editor.fields = [
					{
						type: 'horizontalInput',
						key: 'email',
						templateOptions: {
							type: 'email',
							label: 'Email',
							required: true
						}
					},
					{
						type: 'horizontalInput',
						key: 'firstName',
						templateOptions: {
							type: 'text',
							label: 'Nombre',
							required: true,
							maxlength: 20,
							minlength: 0,
						}
					},
					{
						type: 'horizontalInput',
						key: 'lastName',
						templateOptions: {
							type: 'text',
							label: 'Apellido',
							maxlength: 20,
							minlength: 0,
							required: true
						}
					},
					{
						type: 'horizontalInput',
						key: 'password',
						templateOptions: {
							type: 'text',
							label: 'Contraseña',
							required: true
						},
						expressionProperties: {
							hide: function($viewValue, $modelValue, scope) {
								return !editor.createMode;
							}
						}
					},
					{
						type: 'horizontalInput',
						key: 'dni',
						templateOptions: {
							type: 'text',
							label: 'DNI',
							pattern: '\\d{7,8}',
							required: true
						}
					},
					{
						type: 'horizontalInput',
						key: 'cuil',
						templateOptions: {
							type: 'text',
							label: 'CUIL',
							// pattern: '\\d{1,2}-\\d{7,8}-\\d{1,2}',
							placeholder: '55-55555555-55'
						}
					},
					{
						type: 'horizontalInput',
						key: 'phone',
						templateOptions: {
							type: 'number',
							label: 'Teléfono',
							min: '0',
							required: true
						}
					},
					{
						type: 'horizontalInput',
						key: 'city',
						ngModelAttrs: {
							"": {
								"value": "googleplace"
							},
							"off": {
								"value": "autocomplete"
							}
						},
						templateOptions: {
							type: 'text',
							label: 'Ciudad',
							required: true
						}						
					},
					{
						type: 'horizontalInput',
						key: 'street',
						templateOptions: {
							type: 'text',
							label: 'Domicilio',
							required: true
						}
					},
					{
						type: 'horizontalInput',
						key: 'zip',
						templateOptions: {
							type: 'number',
							label: 'Código postal',
							min: '0',
							minlength: 4,
							maxlength: 4,
							required: true
						}
					},
					{
						type: 'horizontalSwitch',
						key: 'buyGuns',
						value: false,
						templateOptions: {
							label: 'Armas de fuego'
						}
					},
					{
						type: 'horizontalInput',
						key: 'cluaucc',
						templateOptions: {
							type: 'number',
							label: 'CLUAUCC',
							minlength: 0,
						},
						expressionProperties: {
							hide: function($viewValue, $modelValue, scope) {
								if(!editor.user.buyGuns || editor.user.cluauc){
									return true;
								}else{
									return false
								}
								
							}
						}
					},
					{
						type: 'horizontalInput',
						key: 'cluauc',
						templateOptions: {
							type: 'number',
							label: 'CLUAUC',
							minlength: 0,
						},
						expressionProperties: {
							hide: function($viewValue, $modelValue, scope) {
								if(!editor.user.buyGuns || editor.user.cluaucc){
									return true;
								}else{
									return false
								}
								
							}
						}
					},
					{
						type: 'horizontalInput',
						key: 'docket',
						templateOptions: {
							type: 'text',
							label: 'Legajo',
							pattern: '3-\\d{1,2}.\\d{3}.\\d{3}',
							placeholder: '3-55.555.55',
							minlength: 0,
							required: true
						},
						hideExpression: '!model.buyGuns || (!model.cluaucc && !model.cluauc)'
					},
					{
						type: 'horizontalInput',
						key: 'holding',
						templateOptions: {
							type: 'number',
							label: 'Tenencia',
							minlength: 0,
						},
						hideExpression: '!model.buyGuns'
					},
					{
						type: 'horizontalInput',
						key: 'consumerCard',
						templateOptions: {
							type: 'number',
							label: 'Tarjeta de consumo',
							required: true,
							minlength: 0,
						},
						hideExpression: '!model.buyGuns'
					},
					{
						type: 'horizontalInput',
						key: 'cuim',
						templateOptions: {
							type: 'text',
							label: 'CUIM',
							minlength: 0,
						},
						hideExpression: '!model.buyGuns'
					},
					{
						type: 'horizontalInput',
						key: 'serialNumber',
						templateOptions: {
							type: 'number',
							label: 'Número de serie',
							minlength: 0,
						},
						hideExpression: '!model.buyGuns'
					},
					{
						type: 'horizontalTextarea',
						key: 'description',
						templateOptions: {
							type: 'text',
							label: 'Descripción',
							required: false,
							rows: 4
						},
						hideExpression: '!model.buyGuns'
					},
					{
						type: 'horizontalSwitch',
						key: 'buyBullets',
						value: false,
						templateOptions: {
							label: 'Compra municiones'
						}
					},
					{
						type: 'horizontalInput',
						key: 'cluaucc',
						templateOptions: {
							type: 'number',
							label: 'CLUAUCC',
							minlength: 0,
						},
						hideExpression: '!model.buyBullets'
					},
					{
						type: 'horizontalInput',
						key: 'docket',
						templateOptions: {
							type: 'text',
							label: 'Legajo',
							// placeholder: '3-55.555.55',
							minlength: 0,
							required: true
						},
						hideExpression: '!model.buyBullets || !model.cluaucc'
					},
					{
						type: 'horizontalInput',
						key: 'consumerCard',
						templateOptions: {
							type: 'number',
							label: 'Tarjeta de consumo',
							required: true,
							minlength: 0,
						},
						hideExpression: '!model.buyBullets'
					},
					{
						type: 'horizontalTextarea',
						key: 'descriptionBullets',
						templateOptions: {
							type: 'text',
							label: 'Descripción',
							required: false,
							rows: 4
						},
						hideExpression: '!model.buyBullets'
					}
	];

		editor.userCreate = function(user){
			editor.loading = true;
			var newUser = angular.copy(user);
			usersService.userCreate(newUser).then(function(data){
				if(data){
					messageBoxService.showSuccess('Usuario creado correctamente.');
				}else{
					messageBoxService.showError('Ocurrió un error inesperado.');
				}

				$state.go('app.lc.users.list')
				editor.loading = false;

			}, function(error){
				editor.loading = false;

				if(error == 'Error: The specified email address is already in use.'){
					editor.error = 'El email especificado ya está en uso.'
				}
				messageBoxService.showError(editor.error);
			})
		}

		editor.userUpdate = function(user){
			editor.loading = true;
			var updateUser = angular.copy(user);
			usersService.userUpdate(updateUser).then(function(data){
				if(data){
					messageBoxService.showSuccess('Usuario actualizado correctamente.');
				}else{
					messageBoxService.showError('Ocurrió un error inesperado.');
				}
				$state.go('app.lc.users.list')
				editor.loading = false;
			})
		}

		editor.userDelete = function(){
			editor.deleting = true;
			usersService.userRemove(editor.user).then(function(data){
				if(data){
					messageBoxService.showSuccess('Usuario eliminado correctamente.');
				}else{
					messageBoxService.showError('Ocurrió un error inesperado.');
				}
				$state.go('app.lc.users.list')
				editor.deleting = false;
			})
		}
		
		function init() {
			if($state.current.name == 'app.lc.users.create'){
				editor.createMode = true;
			}
		};

		init();
	}

	angular
		.module('users')
			.controller('usersListController', usersListController)
			.controller('usersCreationController', usersEditorController)
			.controller('usersEditionController', usersEditorController)
})()