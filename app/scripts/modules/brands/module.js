(function(){
	'use strict'
	angular.module('brands', [])

	.config(function ($stateProvider, $urlRouterProvider) {

		$stateProvider

			.state('app.lc.brands', {
				url: '/marcas',
				abstract : true,
				template: '<ui-view/>'
			})

			.state('app.lc.brands.list', {
				url: '/lista',
				templateUrl: '/scripts/modules/brands/views/list.html',
				controller: 'brandsListController as list',
				resolve : {
					brandsList : function(brandsService) {
						return brandsService.retrieveAll()
					}
				}
			})

			.state('app.lc.brands.editor', {
				url: '/editar/:brandId',
				templateUrl: '/scripts/modules/brands/views/brand-editor.html',
				controller: 'brandEditorCtrl as editor',
				resolve : {
					brand : function(brandsService, $stateParams) {
						var brandId = $stateParams.brandId;
						return brandsService.getBrandById(brandId)
					}
				}
			})

			.state('app.lc.brands.create', {
				url: '/crear',
				templateUrl: '/scripts/modules/brands/views/brand-editor.html',
				controller: 'brandCreationCtrl as editor',
				resolve: {
					brand : function() {
						return {}
					}
				}
			})
		})
})()
