(function(){

	'use strict'

	var brandsListController = function(brandsList, brandsService, messageBoxService) {
		var list = this;
		list.brands = brandsList;

		list.remove = function(id){
			list.deleteProcess = true;
			console.log(id)
			brandsService.remove(id).then(function(data){
				messageBoxService.showSuccess('Marca eliminada correctamente.');
				list.deleteProcess = false;
			})
		};

	};

	var brandEditorController = function($state, brand, brandsService, messageBoxService) {
		var editor = this;
		brand ? editor.brand = brand : $state.go('app.lc.brands.list');

		editor.save = function(bra){
			editor.loading = true;
			brandsService.brandCreate(bra).then(function(data) {
				if(data){
					messageBoxService.showSuccess('Marca creada correctamente.');
				}else{
					messageBoxService.showError('Ocurrió un error inesperado.');
				}
				$state.go('app.lc.brands.list')
				editor.loading = false;
			})
		}

		editor.update = function(bra){
			editor.loading = true;
			brandsService.brandUpdate(bra).then(function(data) {
				if(data){
					messageBoxService.showSuccess('Marca actualizada correctamente.');
				}else{
					messageBoxService.showError('Ocurrió un error inesperado.');
				}
				$state.go('app.lc.brands.list')
				editor.loading = false;
			})
		}

		function init() {

		};

		init();
	};

	angular.module('brands')
		.controller('brandsListController', brandsListController)
		.controller('brandCreationCtrl', brandEditorController)
		.controller('brandEditorCtrl', brandEditorController)
})();
