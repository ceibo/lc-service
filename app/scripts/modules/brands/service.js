(function(){

	'use strict'

	var brandsService = function(fireRef, Kutral, $q, $firebaseArray, auxFunctions) {
		var service = this;

		var kutral = new Kutral(fireRef);
		var brandSchema;

		var brandModel = kutral.model('brands', {});

		service.retrieveAll = function(){
			var brandRetriveAllPromise = $q.defer();

			var ref = new Firebase(fireRef + 'brands');
			brandRetriveAllPromise.resolve($firebaseArray(ref));

			return brandRetriveAllPromise.promise;
		}

		service.getBrandById = function(id){
			var getBrandByIdPromise = $q.defer();

			brandModel.find({$id:id}, function(data){
				getBrandByIdPromise.resolve(data);
			});

			return getBrandByIdPromise.promise;
		}

		/*service.getBrandByTitle = function(tit){
			var getBrandByTitlePromise = $q.defer();
			brandModel.find({title:tit}, function(data){
				getBrandByTitlePromise.resolve(auxFunctions.resultsToArray(data));
			});

			return getBrandByTitlePromise.promise;
		}*/

		service.getBrandByTitle = function(title){
			var getBrandByTitlePromise = $q.defer(),
				ref = $firebaseArray(new Firebase(fireRef + 'brands')),
				brandFinded;
			ref.$loaded(function(data) {
				data.forEach(function(brand){
					if(brand.title.toLowerCase() == title.toString().toLowerCase()){
						brandFinded = brand;
						return;
					}
				})

				getBrandByTitlePromise.resolve(brandFinded);

			});

			return getBrandByTitlePromise.promise;
		}

		/*service.getBrandByName = function(title){
			var brandGetBrandByNamePromise = $q.defer();

			var brandFinded = [];
			var ref = $firebaseArray(new Firebase(fireRef + 'brands'));
			ref.$loaded(function(data) {
				data.forEach(function(bran){
					if(bran.title && bran.title.toLowerCase() == title.toLowerCase()){
						brandFinded.push(bran)
					}
				})

				brandGetBrandByNamePromise.resolve(brandFinded[0]);

			});

			return brandGetBrandByNamePromise.promise;
		}*/

		service.brandCreate = function(brand){
			var brandCreatePromise = $q.defer(),
				ref = $firebaseArray(new Firebase(fireRef + 'brands'));

			ref.$add(brand).then(function(data) {
				brandCreatePromise.resolve(data.key());
			}, function(error) {
				console.log("Error:", error);
			});

			/*brandModel.data = brand;
			brandModel.create(function(success) {
				if(success){
					brand = brandModel.data;
				}
				brandCreatePromise.resolve(brandModel.data);
			})*/

			return brandCreatePromise.promise;
		}

		service.brandUpdate = function(brand){
			var brandUpdatePromise = $q.defer();

			brandModel.data = brand;
			brandModel.update(function(success) {
				if(success){
					brand = brandModel.data;
				}
				brandUpdatePromise.resolve(brandModel.data);
			})

			return brandUpdatePromise.promise;
		}

		service.remove = function(id){
			var brandRemovePromise = $q.defer();

			brandModel.remove({$id: id},function(success) {
				brandRemovePromise.resolve();
			})

			return brandRemovePromise.promise;
		}
	}

	angular.module('brands')
		.service('brandsService', brandsService)
})();
