(function(){
	'use strict';
	angular.module('main', [])

	.config(function ($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/');

		$stateProvider
			.state('app', {
				abstract: true,
				templateUrl: 'scripts/modules/main/views/main.html',
				controller : 'masterCtrl as master',
				resolve: {
					userData : function(AuthTokenService) {
						return AuthTokenService.getUserData().then(function(data){
							return data
						}, function(error){
							return {}
						});
					}
				}
			})
			
			.state('app.lc', {
				abstract: true,
				template:'<div ui-view="" class="animate-fade-up"></div>'
			})
	})

	.directive('lcHeader', function() {
		return {
			restrict: 'E',
			transclude: true,
			//controller: 'masterCtrl as master',
			templateUrl: 'templates/header.html',
			link: function(scope, element, attrs, controllers) {
				
			}
		};
	})

	.directive('lcFooter', function() {
		return {
			restrict: 'E',
			transclude: true,
			templateUrl: 'templates/footer.html',
			link: function(scope, element, attrs, controllers) {
				
			}
		};
	})

	.directive('lcSidebar', function() {
		return {
			restrict: 'E',
			transclude: true,
			//controller: 'masterCtrl as master',
			templateUrl: 'templates/sidebar.html',
			link: function(scope, element, attrs, controllers) {
				
			}
		};
	})

})()