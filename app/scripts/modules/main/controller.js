(function(){

	'use strict';
	var masterCtrl = function($scope, $rootScope, $state, $window, userData, momentFunctions, AuthTokenService) {
		var master = this;

		master.logOut = function(){
			AuthTokenService.logOut();
			$state.go('auth.login');
		}

		if(!userData || userData.role != 'admin'){
			master.logOut();
		}else{
			master.userData = userData;
		}

		//every 1 min
		setInterval(function(){
			$scope.$apply(function () {
				$rootScope.date = momentFunctions.getDate();
			})
			
		}, 60000)

		master.toggleSidebar = function(){
			master.toggle = !master.toggle;
			master.checkPhone();
		};

		master.setToggle = function(){
			$window.innerWidth <= 767 ? master.toggle = true : master.toggle = false;
		}

		master.checkPhone = function(){
			$window.innerWidth <= 480 && !master.toggle ? master.phone = true : master.phone = false;
		}

		master.updateScrollBar = function(){
			$('.scroller').perfectScrollbar('update');
		}

		function init() {
			master.setToggle();

			var id;
			$(window).resize(function() {
				clearTimeout(id);
				id = setTimeout(function () {
					$scope.$apply(function () {
						master.updateScrollBar()
						master.setToggle();
						master.checkPhone();
					});
				}, 100);
			});

			$rootScope.date = momentFunctions.getDate();
		};

		init();
	}

	angular
		.module('main')
			.controller('masterCtrl', masterCtrl)
})()
