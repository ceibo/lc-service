(function(){
	'use strict';
	angular.module('dashboard', [])

	.config(function ($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('app.lc.dashboard', {
				url: '/',
				templateUrl: '/scripts/modules/dashboard/views/dashboard.html',
				controller : 'dashboardCtrl as dashboard',
				resolve: {
					users : function(usersService) {
						return usersService.getTotal();
					},
					sales : function(salesService){
						return salesService.retrieveAll();
					},
					products : function(productsService){
						return productsService.retrieveLastPublished();
					}
				}
			})
	})
})()