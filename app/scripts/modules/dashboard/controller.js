(function(){

	'use strict'

	var dashboardCtrl = function(users, sales, products, auxFunctions, $rootScope, categoriesService, brandsService, vendorsService) {
		var dashboard = this;
		dashboard.users = users;
		dashboard.sales = sales;
		//dashboard.lastSales = lastSales;
		dashboard.products = products;
		dashboard.salesPending = [];
		dashboard.salesSuccess = [];
		dashboard.alertStock = [];
		dashboard.lastProducts = [];

		dashboard.categoriesService = {
			get: categoriesService.getCategoryById
		}

		dashboard.brandsService = {
			get: brandsService.getBrandById
		}

		dashboard.vendorsService = {
			get: vendorsService.getVendorById
		}

		dashboard.products.reverse();
		//stock
		dashboard.products.forEach(function(product, index){
			if(product.stock <= 5){
				dashboard.alertStock.push(product)
			}
			if(index <= 4){
				dashboard.lastProducts.push(product)
			}

		})

		dashboard.getRevenue = function(sales){
			var total = 0;
			sales.forEach(function(sale){
				sale.items.forEach(function(item){
					if(sale.status == 'success'){
						total += item.price * item.quantity;
					}
				})
			})
			return parseInt(total)
		}


		dashboard.alertStock = auxFunctions.resultsToArray(dashboard.alertStock);
		dashboard.lastProducts = auxFunctions.resultsToArray(dashboard.lastProducts);

		dashboard.sales.forEach(function(sale){
			if(sale.status == 'pending'){
				dashboard.salesPending.push(sale);
			}else{
				dashboard.salesSuccess.push(sale);
			}
		});

		function init () {
			dashboard.revenueTotal = dashboard.getRevenue(dashboard.sales);
		};

		init();
	}

	angular.module('dashboard')
		.controller('dashboardCtrl', dashboardCtrl)

})();