(function(){
	'use strict'
	angular.module('products', [])

	.config(function ($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('app.lc.products', {
				url: '/productos',
				abstract : true,
				template: '<ui-view/>'
			})

			.state('app.lc.products.list', {
				url: '/lista',
				templateUrl: '/scripts/modules/products/views/list.html',
				controller: 'productsListController as list',
				resolve : {
					productsList : function(productsService) {
						return productsService.retrieveAll()
					}
				}
			})

			.state('app.lc.products.editor', {
				url: '/editar/:state/:productId',
				templateUrl: '/scripts/modules/products/views/product-editor.html',
				controller: 'productEditionController as editor',
				resolve : {
					product : function(productsService, categoriesService, $stateParams, $q) {
						var productId = $stateParams.productId;
						var productState = $stateParams.state;
						var productPromise = $q.defer();
						//var promisesArray = [];

						return productsService.getProductById(productId,productState).then(function(product){
							productPromise.resolve(product)
							/*if(product.categories){
								product.categories.forEach(function(id) {
									var loopPromise = $q.defer()
									categoriesService.getCategoryById(id).then(function(data) {
										loopPromise.resolve(data)
									})
										
									promisesArray.push(loopPromise.promise);
								})

							}
							$q.all(promisesArray).then(function(results) {
								productPromise.resolve({product:product, categories: results})
							})*/

							return productPromise.promise;
						})
					},
					categories : function(categoriesService) {
						return categoriesService.retrieveAll()
					},
					subcategories : function(subcategoriesService) {
						return subcategoriesService.retrieveAll()
					},
					vendors : function(vendorsService) {
						return vendorsService.retrieveAll()
					},
					brands : function(brandsService) {
						return brandsService.retrieveAll()
					}
				}
			})

			.state('app.lc.products.create', {
				url: '/crear/',
				templateUrl: '/scripts/modules/products/views/product-editor.html',
				controller: 'productCreationController as editor',
				resolve : {
					product : function(productsService) {
						return productsService.getProductModel()
					},
					categories : function(categoriesService) {
						return categoriesService.retrieveAll()
					},
					subcategories : function(subcategoriesService) {
						return subcategoriesService.retrieveAll()
					},
					vendors : function(vendorsService) {
						return vendorsService.retrieveAll()
					},
					brands : function(brandsService) {
						return brandsService.retrieveAll()
					}
				}
			})

			.state('app.lc.products.editInBlock', {
				url: '/edicion-en-bloque/',
				templateUrl: '/scripts/modules/products/views/product-editor-in-block.html',
				controller: 'productEditorInBlockController as editor',
				resolve : {
					products : function(productsService) {
						return productsService.retrieveAll()
					},
					categories : function(categoriesService) {
						return categoriesService.retrieveAll()
					},
					vendors : function(vendorsService) {
						return vendorsService.retrieveAll()
					},
					brands : function(brandsService) {
						return brandsService.retrieveAll()
					}
				}
			})

			.state('app.lc.products.import', {
				url: '/importar',
				templateUrl: '/scripts/modules/products/views/import.html',
				controller: 'productImportController as importP'
			})
		})

})()
