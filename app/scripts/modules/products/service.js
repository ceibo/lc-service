(function(){

	'use strict'

	var productsService = function($http, fireRef, Kutral, $q, $firebaseArray, $firebaseObject, $stateParams, categoriesService, subcategoriesService, vendorsService, brandsService, auxFunctions) {
		var service = this;

		var kutral;
		kutral = new Kutral(fireRef);
		var productSchema;

		var productModel;
		var publishedProducts;
		var pendingProducts;

		var pendingProductsRef = $firebaseArray(new Firebase(fireRef + 'products/pending'));
		var publishedProductsRef = $firebaseArray(new Firebase(fireRef + 'products/published'));

		var instantiateModels = function() {
			var instantiateModelsPromise = $q.defer();

			productSchema = new Kutral.Schema({
				'title': {type: String, indexOn: true},
				'author': {type: 'ObjectId', ref:'users', indexOn: true},
				'categories': {type: 'ObjectId', ref:'categories', indexOn: true},
				'vendor': {type: 'ObjectId', ref:'vendors', indexOn: true},
				'brand': {type: 'ObjectId', ref:'brands', indexOn: true}
			});

			productModel = kutral.model('products', productSchema);
			publishedProducts = kutral.model.discriminator('published', productModel);
			pendingProducts = kutral.model.discriminator('pending', productModel);
			instantiateModelsPromise.resolve();

			return instantiateModelsPromise.promise;
		};

		service.getProductModel = function() {
			return  {
				publish: true,
				categories : [],
				subcategories : []
			}
		};

		var valueToAutoComplete;
		service.setValueToAutoComplete = function(data){
			console.log(data);
			valueToAutoComplete = data;
		}

		service.getValueToAutoComplete = function(data){
			return data;
		}

		service.retrieveAll = function(){
			var productRetriveAllPromise = $q.defer(), pendingsPromise = $q.defer(), publishedPromise = $q.defer();

			var ref = $firebaseArray(new Firebase(fireRef + 'products/published'));
				ref.$loaded(function(data) {
					if(data) {
						data.forEach(function(element) {
							element.status = 'published';
						});
					}
					publishedPromise.resolve(data);
				});


			var ref = $firebaseArray(new Firebase(fireRef + 'products/pending'));
				ref.$loaded(function(data) {
					if(data) {
						data.forEach(function(element) {
							element.status = 'pending';
						});
					}
					pendingsPromise.resolve(data);
				});


			/*$http.get('https://lascoloradas.firebaseio.com/products/pending.json').success(function(data){
				var array
					if(data){
						array = $.map(data, function(value, index) {
					    	return [value];
						});
					}
				pendingsPromise.resolve(array);
			})

			$http.get('https://lascoloradas.firebaseio.com/products/published.json').success(function(data){
				var array
					if(data){
						array = $.map(data, function(value, index) {
					    	return [value];
						});
					}

				publishedPromise.resolve(array);
			})*/

			$q.all([pendingsPromise.promise, publishedPromise.promise]).then(function(results) {
				var resultsList = [];

				if(results[0] && results[0].length) {
					resultsList= resultsList.concat(results[0]);
				}

				if(results[1] && results[1].length) {
					resultsList = resultsList.concat(results[1]);
				}

				productRetriveAllPromise.resolve(resultsList);
			})

			return productRetriveAllPromise.promise;
		}

		service.retrievePublished = function(){
			var retrievePublishedPromise = $q.defer();

			var ref = $firebaseArray(new Firebase(fireRef + 'products/published'));
				ref.$loaded(function(data) {
					retrievePublishedPromise.resolve(data);
				});

			return retrievePublishedPromise.promise;
		}

		service.getProductById = function(id,state){
			var productGetProductByIdPromise = $q.defer(), pendingsPromise = $q.defer(), publishedPromise = $q.defer();


			instantiateModels().then(function() {
				if(state){
					if(state == 'published'){
						publishedProducts.find({$id:id}, function(data){
							productGetProductByIdPromise.resolve(data);
						});
					}
					if(state == 'pending'){
						pendingProducts.find({$id:id}, function(data){
							productGetProductByIdPromise.resolve(data);
						});
					}
				}else{
					var peRef = $firebaseObject(new Firebase(fireRef + 'products/pending/' + id));
						peRef.$loaded(function(data) {
							if(data === peRef && data.$value !== null){
								productGetProductByIdPromise.resolve(data);
							}
						});

					var ref = $firebaseObject(new Firebase(fireRef + 'products/published/' + id));
						ref.$loaded(function(data) {
							if(data === ref && data.$value !== null){
								productGetProductByIdPromise.resolve(data)
							}
						});
				}
			})


			return productGetProductByIdPromise.promise;
		}

		service.createProduct = function(product, productState) {
			var productCreatePromise = $q.defer();

			if(product.categories) product.categories = service.idsToArray(product.categories);
			if(product.subcategories) product.subcategories = service.idsToArray(product.subcategories);
			if(product.vendor && product.vendor.$id) product.vendor = product.vendor.$id;
			if(product.brand && product.brand.$id) product.brand = product.brand.$id;

			instantiateModels().then(function() {
				if(productState == 'pending' || !productState) {
					pendingProducts.data = product;
					pendingProducts.create(function(success) {
						if(success){
							product = pendingProducts.data;
						}
						productCreatePromise.resolve(pendingProducts.data);
					})
				}

				if(productState == 'published') {
					publishedProducts.data = product;
					publishedProducts.create(function(success) {
						if(success){
							product = publishedProducts.data;
						}
						productCreatePromise.resolve(publishedProducts.data);
					})
				}
			});

			return productCreatePromise.promise;
		};

		service.updateProduct = function(product) {
			var productUpdatePromise = $q.defer();
			var state, oldState;
			product.publish ? state = 'published' : state = 'pending';
			if($stateParams.state){
				oldState = $stateParams.state;
			}else{
				oldState = state
			}
			product.status = state;
			// product.categories = service.idsToArray(product.categories);
			// product.subcategories = service.idsToArray(product.subcategories);

			if(product.categories) product.categories = service.idsToArray(product.categories);
			if(product.subcategories) product.subcategories = service.idsToArray(product.subcategories);

			if(product.vendor){
				if(product.vendor.$id || product.vendor.id){
					(!product.vendor.$id) ? product.vendor = product.vendor.id : product.vendor = product.vendor.$id;
				}
			}
			if(product.brand){
				if(product.brand.$id || product.brand.id){
					(!product.brand.$id) ? product.brand = product.brand.id : product.brand = product.brand.$id;
				}
			}

			if(!product.id && product.$id){
				product.id = product.$id;
			}

			instantiateModels().then(function() {
				if(state == 'published'){
					publishedProducts.data = product;
					publishedProducts.update(function(success) {
						if(success){
							product = publishedProducts.data;
						}
						if(state !== oldState){
							service.removeProduct(product.id, 'pending').then(function(){
								productUpdatePromise.resolve(pendingProducts.data);
							})
						}else{
							productUpdatePromise.resolve(pendingProducts.data);
						}
					})
				}

				if(state == 'pending'){
					pendingProducts.data = product;
					pendingProducts.update(function(success) {
						if(success){
							product = pendingProducts.data;
						}
						if(state !== oldState){
							service.removeProduct(product.id, 'published').then(function(){
								productUpdatePromise.resolve(pendingProducts.data);
							})
						}else{
							productUpdatePromise.resolve(pendingProducts.data);
						}
					})
				}
			});

			return productUpdatePromise.promise

		};

		service.updateProductsInBlock = function(products, newPrice, type){
			var productUpdateInBlockPromise = $q.defer();
			var promisesArrayUpdate = [];

			products.forEach(function(product){
				var loopPromise = $q.defer();
				if(!product.price){
					product.price = 0;
				}
				if(type == 'fixed'){
					product.price = product.price + parseInt(newPrice)
					product.cost = product.cost + parseInt(newPrice)
				}else{
					product.cost = Math.ceil(product.cost + ((product.cost * parseInt(newPrice)) / 100))

					product.price = Math.ceil(product.cost + ((product.cost * parseInt(newPrice)) / 100))

					if(parseInt(newPrice) > 0){
						product.profit_percentage = parseInt(newPrice);
					}
				}

				service.updateProduct(product).then(function(productUpdated){
					loopPromise.resolve(productUpdated)
				})

				promisesArrayUpdate.push(loopPromise.promise);
			})

			$q.all(promisesArrayUpdate).then(function(results) {
				productUpdateInBlockPromise.resolve(results);
			})

			return productUpdateInBlockPromise.promise

		}

		service.deleteProductsInBlock = function(products){
			var productDeleteInBlockPromise = $q.defer(), promisesArrayDelete = [], id;

			products.forEach(function(product){
				var loopPromise = $q.defer();
				!product.id ? id = product.$id : id = product.id;

				service.removeProduct(id, product.status).then(function(productDelete){
					loopPromise.resolve(productDelete)
				})

				promisesArrayDelete.push(loopPromise.promise);
			})

			$q.all(promisesArrayDelete).then(function(results) {
				productDeleteInBlockPromise.resolve(results);
			})

			return productDeleteInBlockPromise.promise

		}

		service.idsToArray = function(product) {
			var categorie = [], categories = [], id;
			product.forEach(function(data) {
				if(data){
					if(data.$id || data.id){
						!data.$id ? id = data.id : id = data.$id;
						categorie.push(id)
					}else{
						categorie.push(data)
					}
				}
			});

			categorie.forEach(function(data) {
				categories.push(data)
			});

			return categories;
		}

		service.removeProduct = function(id, state){
			var productRemovePromise = $q.defer();

			instantiateModels().then(function() {
				if(state == 'published'){
					publishedProducts.remove({$id:id}, function(success) {
						productRemovePromise.resolve(success);
					})
				}

				if(state == 'pending'){
					pendingProducts.remove({$id:id}, function(success) {
						productRemovePromise.resolve(success);
					})
				}
			})
			return productRemovePromise.promise
		}

		service.retrieveLastPublished = function(){
			var retrieveLastPublishedPromise = $q.defer();

			var ref = $firebaseArray(new Firebase(fireRef + 'products/published'));
			ref.$loaded(function(data) {
				retrieveLastPublishedPromise.resolve(data);
			});

			return retrieveLastPublishedPromise.promise;
		}

		service.searchByEAN = function(ean){
			var searchByEANPromise = $q.defer(), productFinded = [], find = false;

			var ref = $firebaseArray(new Firebase(fireRef + 'products/published'));
			ref.$loaded(function(data) {
				data.forEach(function(product){
					if(product.ean == ean){
						find = true;
						productFinded = product
					}
				})

				if(find){
					searchByEANPromise.resolve(productFinded);
				}else{
					var ref = $firebaseArray(new Firebase(fireRef + 'products/pending'));
					ref.$loaded(function(data) {
						data.forEach(function(product){
							if(product.ean == ean){
								find = true;
								productFinded = product
							}
						})

						if(find){
							searchByEANPromise.resolve(productFinded);
						}else{
							searchByEANPromise.resolve();
						}
					});
				}

			});

			return searchByEANPromise.promise;
		}

		service.searchByTitle = function(title){
			var searchByTitlePromise = $q.defer(), productFinded = [], find = false;

			var ref = $firebaseArray(new Firebase(fireRef + 'products/published'));
			ref.$loaded(function(data) {
				data.forEach(function(product){
					if(product.title == title){
						find = true;
						productFinded = product
					}
				})

				if(find){
					searchByTitlePromise.resolve(productFinded);
				}else{
					var ref = $firebaseArray(new Firebase(fireRef + 'products/pending'));
					ref.$loaded(function(data) {
						data.forEach(function(product){
							if(product.title == title){
								find = true;
								productFinded = product
							}
						})

						if(find){
							searchByTitlePromise.resolve(productFinded);
						}else{
							searchByTitlePromise.resolve();
						}
					});
				}

			});

			return searchByTitlePromise.promise;
		}

		service.updateProductStock = function(products, type) {
			var updateProductStockPromise = $q.defer();
			var promisesArrayFind = [];
			var promisesArrayUpdate = [];
			var arrayFind = [];

				products.forEach(function(product){
					if(product.stock){
						var loopPromise = $q.defer();

						service.searchByEAN(product.ean).then(function(productFinded){

							if(productFinded){
								if(type === 'suma'){
									productFinded.stock = productFinded.stock + product.quantity
								}
								if(type === 'resta'){
									productFinded.stock = productFinded.stock - product.quantity
								}

								productFinded.id = productFinded.$id;
							}

							loopPromise.resolve(productFinded)
						})

						promisesArrayFind.push(loopPromise.promise);
					}
				})

				$q.all(promisesArrayFind).then(function(results) {
					var loopPromise = $q.defer();
					results.forEach(function(result){
						result.$id = result.id
						service.updateProduct(result).then(function(productUpdated){
							loopPromise.resolve()
						})

						promisesArrayUpdate.push(loopPromise.promise);
					})
				})

				$q.all(promisesArrayUpdate).then(function(results) {
					updateProductStockPromise.resolve(results);
				})

			return updateProductStockPromise.promise;
		}

		service.readFile = function(file, readCells, toJSON) {
			var readFilePromise = $q.defer();

			XLSXReader(file, readCells, toJSON, function(data) {
				var results = service.getFirst(data['sheets']), categoryArray = [], subcategoryArray = [], resultArray = [], price;

				var resultArray = results.map(function(elm, i) {
					categoryArray = elm['CATEGORÍA']; (categoryArray) ? categoryArray = categoryArray.replace(/^\s+|\s+$/g,"").split(/\s*,\s*/) : categoryArray = [];
					subcategoryArray = elm['SUBCATEGORÍA']; (subcategoryArray) ? subcategoryArray = subcategoryArray.replace(/^\s+|\s+$/g,"").split(/\s*,\s*/) : subcategoryArray = [];
					return { ean: parseInt(elm['EAN']), lcCode: elm['LC-CODE'], vendorCode: elm['VENDOR-CODE'], vendor: elm['VENDOR-NAME'], brand: elm['MARCA'], stock: parseInt(elm['STOCK']), title: elm['NOMBRE DEL PRODUCTO'], cost: Math.ceil(parseInt(elm['PRECIO DE COSTO'])), price: 0, description: elm['DESCRIPCIÓN'], categories : categoryArray, subcategories : subcategoryArray, currency : elm['CURRENCY']};
				});

				var numOfLoops = 1, count = 0, returnToLoop = false, maxProductsPerLoop = 100, products = resultArray;

				if(resultArray.length >= maxProductsPerLoop){
					numOfLoops = Math.ceil(resultArray.length / maxProductsPerLoop)
					products = resultArray.slice(0, maxProductsPerLoop);
				}

				loop()

				function loop(){
					var promisesArrayCreateProduct = [], promisesArrayVendors = [], promisesArrayBrands = [], promisesArrayCategories = [], promisesArraySubcategories = [], promisesArrayFindSubcategories = [];

					count ++;
					products.forEach(function(item, i) {
						//EAN
						if(!item.ean) item.ean = 0;

						//LC-CODE
						if(!item.lcCode) item.lcCode = 0;

						//BRAND
						if(!item.brand) item.brand = '';

						//VENDOR-CODE
						if(!item.vendorCode) item.vendorCode = 0;

						//VENDOR-NAME
						if(!item.vendor) item.vendor = '';

						//STOCK
						if(!item.stock) item.stock = 0;

						//TITLE
						if(!item.title) item.title = '';

						//PRICE
						if(!item.cost) item.cost = 0;

						//DESCRIPTION
						if(!item.description) item.description = '';

						//CURRENCY
						if(!item.currency) item.currency = '';

						//PROVEEDORES
						var loopPromise = $q.defer();
						if(item.vendor){
							//Busco proveedor por nombre
							vendorsService.getVendorByName(item.vendor).then(function(vendor) {
								if(vendor){
									item.vendor = vendor.$id;
									loopPromise.resolve(vendor.$id)

								}else{
									//no se encontró, lo creo.
									var vendorToCreate = {name: item.vendor}
									vendorsService.vendorCreate(vendorToCreate).then(function(newVendor){
										item.vendor = newVendor;
										loopPromise.resolve(newVendor)
									})
								}
							})
						}else{
							item.vendor = '';
							loopPromise.resolve()
						}

						promisesArrayVendors.push(loopPromise.promise);
					})

					$q.all(promisesArrayVendors).then(function() {
						products.forEach(function(item) {
							var loopPromise = $q.defer();
							if(item.brand){
								//Busco marca por nombre
								brandsService.getBrandByTitle(item.brand).then(function(brand) {
									if(brand){
										item.brand = brand.$id;
										loopPromise.resolve(brand)
									}else{
										//no se encontró, la creo.
										var brandToCreate = {title: item.brand}
										brandsService.brandCreate(brandToCreate).then(function(newBrand){
											item.brand = newBrand;
											loopPromise.resolve(item.brand)
										})
									}
								})
							}else{
								item.brand = '';
								loopPromise.resolve()
							}
							promisesArrayBrands.push(loopPromise.promise);
						})
						$q.all(promisesArrayBrands).then(function() {
							products.forEach(function(item, i) {
								if(item.subcategories && item.subcategories.length){
									item.subcategories.forEach(function(subcategory, j) {
										var loopPromise = $q.defer();
										if(subcategory){
											//Busco subcategoría por nombre
											subcategoriesService.getSubcategoryByTitle(subcategory).then(function(subcat) {
												if(subcat){
													products[i].subcategories[j] = subcat.$id;
													loopPromise.resolve(products[i].subcategories[j])
												}else{
													//no se encontró, la creo.
													var subcategoryToCreate = {title: subcategory}
													subcategoriesService.subcategoryCreate(subcategoryToCreate).then(function(newsubcategory){
														products[i].subcategories[j] = newsubcategory;
														loopPromise.resolve(products[i].subcategories[j])
													})
												}
											})
										}else{
											subcategory = '';
											loopPromise.resolve();
										}
										promisesArraySubcategories.push(loopPromise.promise);
									})
								}
							})

							$q.all(promisesArraySubcategories).then(function() {
								products.forEach(function(item, i) {
									if(item.categories && item.categories.length){
										item.categories.forEach(function(category, j) {
											var loopPromise = $q.defer();
											if(category){
												//Busco categoría por nombre
												categoriesService.getCategoryByTitle(category).then(function(cat) {
													if(cat){
														products[i].categories[j] = cat.$id;
														loopPromise.resolve(products[i].categories[j])
													}else{
														//no se encontró, la creo.
														var categoryToCreate = {title: category}
														categoriesService.categoryCreate(categoryToCreate).then(function(newcategory){
															products[i].categories[j] = newcategory;
															loopPromise.resolve(products[i].categories[j])
														})
													}
												})
											}else{
												category = '';
												loopPromise.resolve();
											}
											promisesArrayCategories.push(loopPromise.promise);
										})
									}
								})

								$q.all(promisesArrayCategories).then(function() {

									products.forEach(function(item) {
										if(item.categories && item.categories.length){
											item.categories.forEach(function(category) {
												if(item.subcategories && item.subcategories.length){
													item.subcategories.forEach(function(subcategory) {
														var loopPromise = $q.defer(),
															ref = $firebaseObject(new Firebase(fireRef + 'categories/' + category));

														ref.$loaded().then(function(data) {

				    											if(data.subcategories){
				    												data.subcategories.forEach(function(subcat){
																		data.subcategories.push(subcategory)
																	})
																	// Remove duplicate item from array
				    												data.subcategories = data.subcategories.filter(function(item, pos, self) {
																		return self.indexOf(item) == pos;
																	})
				    											}else{
				    												data.subcategories = [];
				    												data.subcategories.push(subcategory);
				    											}

															ref.$save().then(function() {
																loopPromise.resolve()
															})
			  											})
			  											promisesArrayFindSubcategories.push(loopPromise.promise);

		  											})
												}
											})
										}
									})

									$q.all(promisesArrayFindSubcategories).then(function() {

										pendingProductsRef.$loaded().then(function(){
												products.forEach(function(newproduct) {
													var loopPromise = $q.defer(),productFinded = false, pos;

													if(pendingProductsRef.length){
														pendingProductsRef.forEach(function(product, i) {
															if(product.brand == newproduct.brand && product.vendorCode == newproduct.vendorCode){
																newproduct.$id = pendingProductsRef[i].$id;
																pendingProductsRef[i] = newproduct;
																pos = i;
																productFinded = true;
																return;
															}
														})
													}

													if(productFinded){
														//update
														pendingProductsRef.$save(pos).then(function(data) {
															loopPromise.resolve(data.key())
														})
													}else{
														//create
														pendingProductsRef.$add(newproduct).then(function(data) {
															  loopPromise.resolve(data.key())
															}, function(error) {
															  console.log("Error:", error);
															});
													}

													promisesArrayCreateProduct.push(loopPromise.promise);
												})
										})

										$q.all(promisesArrayCreateProduct).then(function(results) {
											if(numOfLoops == count){
												readFilePromise.resolve(results);
											}else{
												products = resultArray.slice(maxProductsPerLoop*count, maxProductsPerLoop*count + maxProductsPerLoop);
												loop()
											}
										})

									})
								})
							})
						})
					})
				}

			});

			return readFilePromise.promise;
		}

		service.getFirst = function(data){
			var first;
			for (var i in data) {
				if (data.hasOwnProperty(i) && typeof(i) !== 'function') {
					first = data[i];
					break;
				}
			}
			return first
		}

	}

	angular.module('products')
		.service('productsService', productsService)
})();
