(function(){

	'use strict'

	var productsListController = function(productsList, categoriesService, subcategoriesService, brandsService, vendorsService) {
		var list = this;
		list.products = productsList;
		list.allProducts = productsList.length;

		list.categoriesService = {
			get: categoriesService.getCategoryById
		}

		list.subcategoriesService = {
			get: subcategoriesService.getSubcategoryById
		}

		list.brandsService = {
			get: brandsService.getBrandById
		}

		list.vendorsService = {
			get: vendorsService.getVendorById
		}
		
		function init() {
		
		};

		init();
	};

	var productEditorInBlockController = function(products, categories, vendors, brands, productsService, messageBoxService, $state, categoriesService, subcategoriesService, brandsService, vendorsService) {
		var editor = this;
		editor.rowCollection = angular.copy(products);
		editor.categories = categories;
		editor.vendors = vendors;
		editor.brands = brands;
		editor.type = 'fixed';

		editor.categoriesService = {
			get: categoriesService.getCategoryById
		}

		editor.subcategoriesService = {
			get: subcategoriesService.getSubcategoryById
		}

		editor.brandsService = {
			get: brandsService.getBrandById
		}

		editor.vendorsService = {
			get: vendorsService.getVendorById
		}

		editor.products = [].concat(editor.rowCollection);

		editor.selectItems = [{"value":"10"},{"value":"20"},{"value":"50"},{"value":"100"},{"value":"Todos"}];
		editor.itemsByPage = editor.selectItems[1];

		editor.setItemsByPage =  function(){
			if(editor.itemsByPage.value == 'Todos'){
				editor.itemsByPage = {"value": products.length}
			}
		}

		editor.filterProducts = function(){
			var productArray = [];
			products.forEach(function(product){
				
				//only one
				if(editor.selectedVendor && !editor.selectedCategory && !editor.selectedBrand && product.vendor == editor.selectedVendor.$id){
					productArray.push(product)
				}

				if(editor.selectedCategory && !editor.selectedVendor && !editor.selectedBrand && editor.findCategory(product, editor.selectedCategory.$id)){
					productArray.push(product)
				}

				if(editor.selectedBrand && !editor.selectedVendor && !editor.selectedCategory && product.brand == editor.selectedBrand.$id){
					productArray.push(product)
				}

				//two
				if(editor.selectedVendor && editor.selectedCategory && !editor.selectedBrand && product.vendor == editor.selectedVendor.$id && editor.findCategory(product, editor.selectedCategory.$id)){
					productArray.push(product)
				}

				if(editor.selectedVendor && editor.selectedBrand && !editor.selectedCategory && product.vendor == editor.selectedVendor.$id && product.brand == editor.selectedBrand.$id){
					productArray.push(product)
				}

				if(editor.selectedCategory && editor.selectedBrand && !editor.selectedVendor && editor.findCategory(product, editor.selectedCategory.$id) && product.brand == editor.selectedBrand.$id){
					productArray.push(product)
				}

				//all selected
				if(editor.selectedVendor && editor.selectedCategory && editor.selectedBrand){
					if(product.vendor == editor.selectedVendor.$id && editor.findCategory(product, editor.selectedCategory.$id) && product.brand == editor.selectedBrand.$id){
						productArray.push(product)
					}
				}
			})
			editor.rowCollection = productArray;
		}

		editor.findCategory = function(product, id){
			var find = false;
			product.categories.forEach(function(category){
				if(category == id){
					find = true;
				}
			})
			return find;
		}

		editor.reset = function(){
			editor.rowCollection = products;
			editor.selectedVendor = '';
			editor.selectedCategory = '';
			editor.selectedBrand = '';
			editor.itemsByPage = editor.selectItems[1];
			editor.selectedFromCkeckbox = [];
		}

		editor.update = function(){
			editor.loading = true;
			if(editor.rowCollection.length){
				var products = angular.copy(editor.rowCollection);
				productsService.updateProductsInBlock(products, editor.priceToUpdate, editor.type).then(function(){
					messageBoxService.showSuccess('Datos actualizados correctamente.');
					$state.go($state.current, {}, {reload: true});
					editor.loading = false;
				})
			}
		}

		editor.delete = function(){
			editor.deleting = true;
			if(editor.rowCollection.length){
				if(editor.selectedFromCkeckbox && editor.selectedFromCkeckbox.length){
					var products = angular.copy(editor.selectedFromCkeckbox);
				}else{
					var products = angular.copy(editor.rowCollection);
				}
				productsService.deleteProductsInBlock(products).then(function(){
					messageBoxService.showSuccess('Productos eliminados correctamente.');
					$state.go($state.current, {}, {reload: true});
					editor.deleting = false;
				})
			}
		}

		function init() {
		
		};

		init();
	};


	var productEditorController = function($scope, product, categories, subcategories, vendors, brands, productsService, expensesService, $state, $stateParams, auxFunctions, momentFunctions, messageBoxService, categoriesService, subcategoriesService, brandsService, vendorsService) {
		var editor = this;
		product ? editor.product = product : $state.go('app.lc.products.list');

		editor.categoriesService = {
			get: categoriesService.getCategoryById
		}

		editor.subcategoriesService = {
			get: subcategoriesService.getSubcategoryById
		}

		editor.brandsService = {
			get: brandsService.getBrandById
		}

		editor.vendorsService = {
			get: vendorsService.getVendorById
		}

		editor.setPriceWithProfit = function(){
			if(editor.product.cost && editor.product.profit_percentage){
				editor.product.price = Math.round(editor.product.cost + ((editor.product.cost * editor.product.profit_percentage) / 100));
			}
		}

		editor.checkEan = function(ean, product){
			if(ean && ean.toString().length == 13){
				productsService.searchByEAN(ean).then(function(data) {
					if(data && product.title !== data.title){
						editor.eanFinded = true;
					}else{
						editor.eanFinded = false;
					}
				})
			}
		}

		editor.createProduct = function(data){
			editor.loading = true;
			var publish;
			data.publish ? publish = 'published' : publish = 'pending';
			editor.expenses(data);
			var newData = angular.copy(data);
			productsService.createProduct(newData, publish).then(function() {
				messageBoxService.showSuccess('Producto creado correctamente.');
				$state.go('app.lc.products.list')
				editor.loading = false;
			})
		}

		editor.updateProduct = function(data){
			editor.loading = true;
			editor.expenses(data);
			var newData = angular.copy(data);
			productsService.updateProduct(newData).then(function() {
				messageBoxService.showSuccess('Producto actualizado correctamente.');
				$state.go('app.lc.products.list')
				editor.loading = false;
			})
		}

		editor.expenses = function(data){
			var stockFin = data.stock;
			if(stockFin - editor.stockInit > 0 && editor.isEgreso) {
				var expenseData = [];
				expenseData.push({"cost" : (stockFin - editor.stockInit) * data.cost, "date" : momentFunctions.getDate()});
				expensesService.expensesCreate(expenseData[0]).then(function(){})
			}
		}

		/*editor.addCategory = function(newCategory) {
			auxFunctions.addElement(editor.product.categories, newCategory, "description", { notAllowRepeated: true });
		};

		editor.removeCategory = function(category){
			auxFunctions.removeElement(editor.product.categories, category);
		};*/

		editor.deleteProduct = function(){
			editor.deleting = true;
			productsService.removeProduct(editor.product.id, $stateParams.state).then(function(data){
				messageBoxService.showSuccess('Producto eliminado correctamente.');
				$state.go('app.lc.products.list')
				editor.deleting = false;
			})
		}

		editor.getSubcategoriesForCategory = function(categories){
			editor.subcategories = [];
			categories.forEach(function(categorie){
				if(categorie.subcategories){
					categorie.subcategories.forEach(function(subcategorie){
						subcategories.forEach(function(sub){
							if(subcategorie == sub.$id || subcategorie == sub.id){
								editor.subcategories.push(sub);
								// Remove duplicate item from array 
								editor.subcategories = editor.subcategories.filter(function(item, pos, self) {
									return self.indexOf(item) == pos;
								})
							}
						})
					})
				}
			})

		}

		$scope.$watch("editor.product.categories", function(value, oldVal) {
			editor.getSubcategoriesForCategory(value)
			
		}, true);

		function init () {

			editor.selectCurrency = [];

			editor.stockInit = editor.product.stock || 0;
			editor.product.subcategories = editor.product.subcategories || [];
			editor.product.categories = editor.product.categories || [];

			editor.categories = categories;
			editor.vendors = vendors;
			editor.brands = brands;
			editor.products = [];
		};

		init();
	}

	var productImportController = function(productsService, $state, messageBoxService) {
		var importP = this;

		importP.readCells = true;
		importP.toJSON = true;

		importP.uploadFile = function() {
			importP.loading = true;
			productsService.readFile(importP.file, importP.readCells, importP.toJSON).then(function(data) {
				importP.loading = false;
				$state.go($state.current, {}, {reload: true});
				messageBoxService.showSuccess('Productos cargados correctamente.');
			});
		}

		 function init () {
			
		};

		init();
	}

	angular.module('products')
		.controller('productsListController', productsListController)
		.controller('productCreationController', productEditorController)
		.controller('productEditionController', productEditorController)
		.controller('productEditorInBlockController', productEditorInBlockController)
		.controller('productImportController', productImportController)

})();