var categoriesAutocompleteCtrl  = function($resource) {
	var ctrl = this;

	if(ctrl.id){
		ctrl.categoriesArray = [];

		if(ctrl.service) {
			ctrl.loading = true;
			if(ctrl.serviceAsPromise) {
				ctrl.id.forEach(function(id){
					ctrl.service.get(id)
						.then(function(data){
							if(data){
								ctrl.categoriesArray.push(data);
								ctrl.model = ctrl.categoriesArray;
							}else{
								ctrl.model = [];
							}
							ctrl.loading = false;
						})
					;
				})	
			} else {
				ctrl.id.forEach(function(id){
					ctrl.service.get(id, function(data){
						if(data){
							ctrl.categoriesArray.push(data);
							ctrl.model = ctrl.categoriesArray;
						}else{
							ctrl.model = [];
						}
						ctrl.loading = false;
					});
				})
			}
		} else {

			ctrl.getCategorie = function(){
				ctrl.loading = true;
				ctrl.id.forEach(function(id){
					var Categories = $resource(ctrl.endpoint + id);
					Categories.get({}, function success(data) {
						if(data){
							ctrl.categoriesArray.push(data);
							ctrl.model = ctrl.categoriesArray;
						}else{
							ctrl.model = [];
						}
						ctrl.loading = false;
					}, function error(error){
						console.log(error);
						ctrl.loading = false;
					})
				})
			}

			ctrl.deleteCategorie = function() {
				Categories.delete({}, function success(data) {
					if(data){
						ctrl.categoriesArray.push(data);
						ctrl.model = ctrl.categoriesArray;
					}else{
						ctrl.model = [];
					}
				}, function error(error){
					console.log(error);	
				})	

			}

			ctrl.getCategorie();	
		}
	}else{
		//no id
	}

	//functions

	ctrl.addCategory = function(newCategory){
		ctrl.model.push(newCategory)
	}

	ctrl.checkRepeat = function(title){
		var find;
		ctrl.model.forEach(function(categorie){
			if(categorie.title == title){
				find = true;
			}
		})

		return find
	}

	ctrl.removeCategory = function(category){
		var index = ctrl.model.indexOf(category);
		ctrl.model.splice(index, 1);
	};
};

angular
	.module('lc.components.categories.autocomplete', [])
	.controller('categoriesAutocompleteCtrl', categoriesAutocompleteCtrl)
	.directive('categoriesAutocomplete', function() {
        // Runs during compile
        return {
            scope: {
                id: '=',
                endpoint: '=', 
                service: '=',
                serviceAsPromise : '=',
                placeholder: '=',
                model: '=',
                categories: '=',
                required: '='
            }, // {} = isolate, true = child, false/undefined = no change
            controller: 'categoriesAutocompleteCtrl as ctrl',
            //require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
            restrict: 'EA', // E = Element, A = Attribute, C = Class, M = Comment
            templateUrl: '../scripts/components/categories/templates/autocomplete.html',
            bindToController: true
        };
    });
