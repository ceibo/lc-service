var categoriesDisplayerCtrl  = function($resource) {
	var ctrl = this;	
	var queryParams = {}
	queryParams[ ctrl.idAs || 'id'] = ctrl.id;
	if(ctrl.id){
		var Categories = $resource(ctrl.endpoint + ctrl.id);

		if(ctrl.service) {
			ctrl.loading = true;
			if(ctrl.serviceAsPromise) {
				ctrl.service.get(ctrl.id)
					.then(function(data){
						if(data){
							ctrl.data = data[ctrl.categoryKey];
						}
						ctrl.loading = false;
					})
				;	
			} else {
				ctrl.service.get(ctrl.id, function(data){
					if(data){
						ctrl.data = data[ctrl.categoryKey];
					}
					ctrl.loading = false;
				});
			}
		} else {
			ctrl.getCategory = function(){
				ctrl.loading = true;
				Categories.get({}, function success(data) {
					if(data){
						ctrl.data = data;
					}
					ctrl.loading = false;
				}, function error(error){
					console.log(error);	
					ctrl.loading = false;
				})
			}

			ctrl.deleteCategory = function() {
				Categories.delete({}, function success(data) {
					if(data){
						ctrl.data = data;
					}
				}, function error(error){
					console.log(error);	
				})	

			}

			ctrl.getCategory();	
		}
	}else{
		//no id
	}

};

angular
	.module('lc.components.categories.displayer', [])
	.controller('categoriesDisplayerCtrl', categoriesDisplayerCtrl)
	.directive('categoriesDisplayer', function() {
        // Runs during compile
        return {
            scope: {
                id: '=',
                endpoint: '=', 
                idAs : '=',
                service: '=',
                serviceAsPromise : '=',
                categoryKey: '='
            }, // {} = isolate, true = child, false/undefined = no change
            controller: 'categoriesDisplayerCtrl as ctrl',
            // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
            restrict: 'EA', // E = Element, A = Attribute, C = Class, M = Comment
            template: '<span ng-if="ctrl.data.length"> {{ ctrl.data }} </span><span ng-if="!ctrl.data.length && !ctrl.loading">Categoria no disponible</span><span ng-if="ctrl.loading"><i class="fa fa-spinner fa-spin"></i></span><span ng-if="!ctrl.id">Sin categoría</span>',
            bindToController: true
        };
    });
