var subcategoriesAutocompleteCtrl  = function($resource) {
	var ctrl = this;

	if(ctrl.id){
		ctrl.subcategoryArray = [];

		if(ctrl.service) {
			ctrl.loading = true;
			if(ctrl.serviceAsPromise) {
				ctrl.id.forEach(function(id){
					ctrl.service.get(id)
						.then(function(data){
							if(data){
								ctrl.subcategoryArray.push(data);
								ctrl.model = ctrl.subcategoryArray;
							}else{
								ctrl.model = [];
							}
							ctrl.loading = false;
						})
					;
				})	
			} else {
				ctrl.id.forEach(function(id){
					ctrl.service.get(id, function(data){
						if(data){
							ctrl.subcategoryArray.push(data);
							ctrl.model = ctrl.subcategoryArray;
						}else{
							ctrl.model = [];
						}
						ctrl.loading = false;
					});
				})
			}
		} else {

			ctrl.getSubcategorie = function(){
				ctrl.loading = true;
				ctrl.id.forEach(function(id){
					var Subcategories = $resource(ctrl.endpoint + id);
					Subcategories.get({}, function success(data) {
						if(data){
							ctrl.subcategoryArray.push(data);
							ctrl.model = ctrl.subcategoryArray;
						}else{
							ctrl.model = [];
						}
						ctrl.loading = false;
					}, function error(error){
						console.log(error);
						ctrl.loading = false;
					})
				})
			}

			ctrl.deleteSubcategorie = function() {
				Subcategories.delete({}, function success(data) {
					if(data){
						ctrl.subcategoryArray.push(data);
						ctrl.model = ctrl.subcategoryArray;
					}else{
						ctrl.model = [];
					}
				}, function error(error){
					console.log(error);	
				})	

			}

			ctrl.getSubcategorie();	
		}
	}else{
		//no id
	}

	//functions

	ctrl.addSubcategory = function(newSubcategory){
		if(ctrl.model){
			ctrl.model.push(newSubcategory)
		}
	};

	ctrl.checkRepeat = function(title){
		var find;
		if(ctrl.model){
			ctrl.model.forEach(function(subcategorie){
				if(subcategorie.title == title){
					find = true;
				}
			})
		}
		return find
	}

	ctrl.removeSubcategory = function(category){
		if(ctrl.model){
			var index = ctrl.model.indexOf(category);
			ctrl.model.splice(index, 1);
		}
	};
};

angular
	.module('lc.components.subcategories.autocomplete', [])
	.controller('subcategoriesAutocompleteCtrl', subcategoriesAutocompleteCtrl)
	.directive('subcategoriesAutocomplete', function() {
        // Runs during compile
        return {
            scope: {
                id: '=',
                endpoint: '=', 
                service: '=',
                serviceAsPromise : '=',
                placeholder: '=',
                model: '=',
                subcategories: '=',
                disabled: '=',
                required: '='
            }, // {} = isolate, true = child, false/undefined = no change
            controller: 'subcategoriesAutocompleteCtrl as ctrl',
            //require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
            restrict: 'EA', // E = Element, A = Attribute, C = Class, M = Comment
            templateUrl: '../scripts/components/subcategories/templates/autocomplete.html',
            bindToController: true
        };
    });
