var brandsDisplayerCtrl  = function($resource) {
	var ctrl = this;	
	var queryParams = {}
	
	queryParams[ ctrl.idAs || 'id'] = ctrl.id;

	if(ctrl.id){
		var Brands = $resource(ctrl.endpoint + ctrl.id);

		if(ctrl.service) {
			ctrl.loading = true;
			if(ctrl.serviceAsPromise) {
				ctrl.service.get(ctrl.id)
					.then(function(data){
						if(data){
							ctrl.data = data[ctrl.brandKey];
						}
						ctrl.loading = false;
					})
				;	
			} else {
				ctrl.service.get(ctrl.id, function(data){
					if(data){
						ctrl.data = data[ctrl.brandKey];
					}
					ctrl.loading = false;
				});
			}
		} else {
			ctrl.getBrand = function(){
				ctrl.loading = true;
				Brands.get({}, function success(data) {
					if(data){
						ctrl.data = data;
					}
					ctrl.loading = false;
				}, function error(error){
					console.log(error);
					ctrl.loading = false;
				})
			}

			ctrl.deleteBrand = function() {
				Brands.delete({}, function success(data) {
					if(data){
						ctrl.data = data;
					}
				}, function error(error){
					console.log(error);	
				})	

			}

			ctrl.getBrand();	
		}
	}else{
		//no id;
		
	}

};

angular
	.module('lc.components.brands.displayer', [])
	.controller('brandsDisplayerCtrl', brandsDisplayerCtrl)
	.directive('brandsDisplayer', function() {
        // Runs during compile
        return {
            scope: {
                id: '=',
                endpoint: '=', 
                idAs : '=',
                service: '=',
                serviceAsPromise : '=',
                brandKey: '='
            }, // {} = isolate, true = child, false/undefined = no change
            controller: 'brandsDisplayerCtrl as ctrl',
            // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
            restrict: 'EA', // E = Element, A = Attribute, C = Class, M = Comment
            template: '<span ng-if="ctrl.data.length"> {{ ctrl.data }} </span><span ng-if="!ctrl.data.length && !ctrl.loading">Marca no disponible</span><span ng-if="ctrl.loading"><i class="fa fa-spinner fa-spin"></i></span>',
            bindToController: true
        };
    });
