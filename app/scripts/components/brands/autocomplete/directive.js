var brandsAutocompleteCtrl  = function($resource, productsService) {
	var ctrl = this;

	if(ctrl.id){
		var Vendors = $resource(ctrl.endpoint + ctrl.id);

		if(ctrl.service) {
			ctrl.loading = true;
			if(ctrl.serviceAsPromise) {
				ctrl.service.get(ctrl.id)
					.then(function(data){
						if(data){
							ctrl.model = data;
						}else{
							ctrl.model = '';
						}
						ctrl.loading = false;
					})
				;	
			} else {
				ctrl.service.get(ctrl.id, function(data){
					if(data){
						ctrl.model = data;
					}else{
						ctrl.model = '';
					}
					ctrl.loading = false;
				});
			}
		} else {
			ctrl.getVendor = function(){
				ctrl.loading = true;
				Vendors.get({}, function success(data) {
					if(data){
						ctrl.model = data;
					}else{
						ctrl.model = '';
					}
					ctrl.loading = false;
				}, function error(error){
					console.log(error);
					ctrl.loading = false;
				})
			}

			ctrl.deleteVendor = function() {
				Vendors.delete({}, function success(data) {
					if(data){
						ctrl.model = data;
					}else{
						ctrl.model = '';
					}
				}, function error(error){
					console.log(error);	
				})	

			}

			ctrl.getVendor();	
		}
	}else{
		//no id
	}
};

angular
	.module('lc.components.brands.autocomplete', [])
	.controller('brandsAutocompleteCtrl', brandsAutocompleteCtrl)
	.directive('brandsAutocomplete', function() {
        // Runs during compile
        return {
            scope: {
                id: '=',
                endpoint: '=', 
                service: '=',
                serviceAsPromise : '=',
                placeholder: '=',
                model: '=',
                brands: '=',
                required: '='
            }, // {} = isolate, true = child, false/undefined = no change
            controller: 'brandsAutocompleteCtrl as ctrl',
            //require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
            restrict: 'EA', // E = Element, A = Attribute, C = Class, M = Comment
            templateUrl: '../scripts/components/brands/templates/autocomplete.html',
            bindToController: true
        };
    });
