var productsDisplayerCtrl  = function($scope, $resource) {
	var ctrl = this;	
	var queryParams = {}
	
	queryParams[ ctrl.idAs || 'id'] = ctrl.id;

	if(ctrl.id){
		var Products = $resource(ctrl.endpoint + ctrl.id);
		if(ctrl.service) {
			ctrl.loading = true;
			if(ctrl.serviceAsPromise) {
				ctrl.service.get(ctrl.id)
					.then(function(data){
						if(data){
							ctrl.data = data[ctrl.productKey];
						}
						ctrl.loading = false;
					})
				;	
			} else {
				ctrl.service.get(ctrl.id, function(data){
					if(data){
						ctrl.data = data[ctrl.productKey];
					}
					ctrl.loading = false;
				});
			}
		} else {
			ctrl.getProduct = function(){
				ctrl.loading = true;
				Products.get({}, function success(data) {
					if(data){
						ctrl.data = data;
					}
					ctrl.loading = false;
				}, function error(error){
					console.log(error);
					ctrl.loading = false;
				})
			}

			ctrl.deleteProduct = function() {
				Products.delete({}, function success(data) {
					if(data){
						ctrl.data = data;
					}
				}, function error(error){
					console.log(error);	
				})	

			}

			ctrl.getProduct();	
		}
	}else{
		//no id;
		
	}

};

angular
	.module('lc.components.products.displayer', [])
	.controller('productsDisplayerCtrl', productsDisplayerCtrl)
	.directive('productsDisplayer', function() {
        // Runs during compile
        return {
            scope: {
                id: '=',
                endpoint: '=', 
                idAs : '=',
                service: '=',
                serviceAsPromise : '=',
                productKey: '='
            }, // {} = isolate, true = child, false/undefined = no change
            controller: 'productsDisplayerCtrl as ctrl',
            // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
            restrict: 'EA', // E = Element, A = Attribute, C = Class, M = Comment
            template: '<span ng-if="ctrl.data.length"> {{ ctrl.data }} </span><span ng-if="!ctrl.data.length && !ctrl.loading">Producto no disponible</span><span ng-if="ctrl.loading"><i class="fa fa-spinner fa-spin"></i></span>',
            bindToController: true
        };
    });
