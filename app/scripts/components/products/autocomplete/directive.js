var productsAutocompleteCtrl  = function($scope, $resource, productsService) {
	var ctrl = this;

		// $scope.$watch("ctrl.ngModel", function(value, oldVal) {
		// 	if(value && value.$id){
		// 		productsService.setValueToAutoComplete(value);
		// 	}
			
		// }, true);

	if(ctrl.id){
		var Products = $resource(ctrl.endpoint + ctrl.id);
		if(ctrl.service) {
			ctrl.loading = true;
			if(ctrl.serviceAsPromise) {
				ctrl.service.get(ctrl.id)
					.then(function(data){
						if(data){
							ctrl.ngModel = data;
						}else{
							ctrl.ngModel = '';
						}
						ctrl.loading = false;
					})
				;	
			} else {
				ctrl.service.get(ctrl.id, function(data){
					if(data){
						ctrl.ngModel = data;
					}else{
						ctrl.ngModel = '';
					}
					ctrl.loading = false;
				});
			}
		} else {
			ctrl.getProduct = function(){
				ctrl.loading = true;
				Products.get({}, function success(data) {
					if(data){
						ctrl.ngModel = data;
					}else{
						ctrl.ngModel = '';
					}
					ctrl.loading = false;
				}, function error(error){
					console.log(error);
					ctrl.loading = false;
				})
			}

			ctrl.deleteProduct = function() {
				Products.delete({}, function success(data) {
					if(data){
						ctrl.ngModel = data;
					}else{
						ctrl.ngModel = '';
					}
				}, function error(error){
					console.log(error);	
				})	

			}

			ctrl.getProduct();	
		}
	}else{
		//no id


	}
};

angular
	.module('lc.components.products.autocomplete', [])
	.controller('productsAutocompleteCtrl', productsAutocompleteCtrl)
	.directive('productsAutocomplete', function() {
        // Runs during compile
        return {
            scope: {
                id: '=',
                endpoint: '=', 
                service: '=',
                serviceAsPromise : '=',
                placeholder: '=',
                ngModel: '=',
                products: '=',
                required: '='
            }, // {} = isolate, true = child, false/undefined = no change
            controller: 'productsAutocompleteCtrl as ctrl',
            //require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
            restrict: 'EA', // E = Element, A = Attribute, C = Class, M = Comment
            templateUrl: '../scripts/components/products/templates/autocomplete.html',
            bindToController: true
        };
    });
