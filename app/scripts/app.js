(function(){

'use strict';

angular
  .module('lascoloradasApp', [
    'ceibo.ui',
    'ui.bootstrap',
    'mgcrea.ngStrap',
    'ui.router',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngStorage',
    'smart-table',
    'froala',
    'datePicker',
    'angularMoment',
    'fireQuery',
    'nvd3',
    'auth',
    'main',
    'about',
    'users',
    'utils',
    'sales',
    'brands',
    'vendors',
    'economy',
    'products',
    'revenues',
    'expenses',
    'dashboard',
    'categories',
    'subcategories',
    'fileReaderModule',
    'perfect_scrollbar',
    'angular-loading-bar',
    'perfect_scrollbar',
    'fireServiceModule',
    'angularFileUpload',
    'ngFileUpload',
    'formly',
    'formlyBootstrap',
    'ngMessages',
    'logglyLogger',
    'ceibo.components.table.export',
    //UI
    'lc.serv.ui.photo-uploader',
    'lc.serv.ui.list-photo-uploader',
    //components
    'lc.components.products.displayer',
    'lc.components.products.autocomplete',
    'lc.components.categories.displayer',
    'lc.components.categories.autocomplete',
    'lc.components.subcategories.displayer',
    'lc.components.subcategories.autocomplete',
    'lc.components.brands.displayer',
    'lc.components.brands.autocomplete',
    'lc.components.vendors.displayer',
    'lc.components.vendors.autocomplete'
  ])

  .constant('fireRef', 'https://lascoloradas.firebaseio.com/')
  .constant('LoggyToken', 'c1aab539-7d18-4661-b349-48790dd68949')
  .constant('utils', 'https://lascoloradas-utils.herokuapp.com')

  .run(function ($state, $rootScope, formlyConfig, formlyValidationMessages) {
    $rootScope.$state = $state;

    formlyConfig.setType({
      name: 'horizontalInput',
      extends: 'input',
      controller: ['$scope', function($scope) {
        $scope.options.data.getValidationMessage = getValidationMessage;

        function getValidationMessage(key) {
          var message = $scope.options.validation.messages[key];
          if (message) {
            return message($scope.fc.$viewValue, $scope.fc.$modelValue, $scope);
          }
        }
      }]
    });

    formlyConfig.setType({
      name: 'horizontalTextarea',
      extends: 'textarea',
      controller: ['$scope', function($scope) {
        $scope.options.data.getValidationMessage = getValidationMessage;

        function getValidationMessage(key) {
          var message = $scope.options.validation.messages[key];
          if (message) {
            return message($scope.fc.$viewValue, $scope.fc.$modelValue, $scope);
          }
        }
      }]
    });

    formlyConfig.setType({
      name: 'typeahead',
      extends: 'input',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError'],
      template: '<input ng-model="model[options.key]" uib-typeahead="item for item in to.options | filter:$viewValue | limitTo:8" class="form-control">',
      controller: ['$scope', function($scope) {
        $scope.options.data.getValidationMessage = getValidationMessage;

        function getValidationMessage(key) {
          var message = $scope.options.validation.messages[key];
          if (message) {
            return message($scope.fc.$viewValue, $scope.fc.$modelValue, $scope);
          }
        }
      }]
    });

    formlyConfig.setWrapper({
      name: 'validation',
      types: ['input', 'textarea', 'horizontalInput', 'horizontalTextarea', 'horizontalInputFuntions', 'typeahead'],
      templateUrl: 'templates/formly-custom/messages.html'
    });

    formlyConfig.setWrapper({
      name: 'horizontalBootstrapLabel',
      template: [
        '<label for="{{::id}}" class="col-md-2 control-label">',
          '{{to.label}} {{to.required ? "*" : ""}}',
        '</label>',
        '<div class="col-sm-8 col-lg-7">',
          '<formly-transclude></formly-transclude>',
        '</div>'
      ].join(' ')
    });

    formlyConfig.setType({
      name: 'horizontalSwitch',
      extends: 'checkbox',
      overwriteOk: true,
      templateUrl: 'templates/formly-custom/horizontalBootstrapSwitch.html'
    });

    formlyConfig.setType({
      name: 'horizontalCheckbox',
      extends: 'checkbox',
      overwriteOk: true,
      templateUrl: 'templates/formly-custom/horizontalBootstrapCheckbox.html'
    });


    formlyConfig.setType({
      name: 'horizontalInputFuntions',
      extends: 'input',
      overwriteOk: true,
      templateUrl: 'templates/formly-custom/input-template-functions.html',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfig.setType({
      name: 'horizontalInput',
      extends: 'input',
      overwriteOk: true,
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfig.setType({
      name: 'horizontalTextarea',
      extends: 'textarea',
      overwriteOk: true,
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfig.removeWrapperByName('bootstrapLabel');
    formlyConfig.setWrapper({
      name: 'bootstrapLabel',
      templateUrl: 'templates/formly-custom/label-wrapper.html'
    });

    // Replace formlyBootstrap input field type to implement read-only forms
    formlyConfig.setType({
      name: 'horizontalInputReadOnly',
      templateUrl: 'templates/formly-custom/input-template-read-only.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError'],
      overwriteOk: true
    });

    formlyValidationMessages.addTemplateOptionValueMessage('maxlength', 'maxlength', '', 'es la longitud máxima');
    formlyValidationMessages.addTemplateOptionValueMessage('minlength', 'minlength', '', 'es la longitud mínima');
    formlyValidationMessages.addTemplateOptionValueMessage('max', 'max', '', 'es el valor máximo');
    formlyValidationMessages.addTemplateOptionValueMessage('min', 'min', '', 'es el valor mínimo');
    formlyValidationMessages.messages.required = 'to.label + " es un campo requerido"';
    formlyValidationMessages.messages.pattern = '"Formato inválido"';
    formlyValidationMessages.messages.number = 'to.label + " debe ser un número"';
    formlyValidationMessages.messages.email = '$viewValue + " no es un email válido"';
  })

  .config(['$stateProvider','$urlRouterProvider', '$httpProvider', '$provide', 'LogglyLoggerProvider', 'LoggyToken',
    function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, LogglyLoggerProvider, LoggyToken) {
    $httpProvider.interceptors.push('AuthInterceptor');

        //negative numbers
        $provide.decorator('$locale', ['$delegate', function($delegate) {
          if($delegate.id == 'en-us') {
            $delegate.NUMBER_FORMATS.PATTERNS[1].negPre = '-\u00A4';
            $delegate.NUMBER_FORMATS.PATTERNS[1].negSuf = '';
          }
          return $delegate;
        }]);

    LogglyLoggerProvider
        .inputToken(LoggyToken)
        .sendConsoleErrors(true)
        .includeTimestamp(true);
  }])

})()
